# `be-wal`: FaST Platform (Wallonia)

<img src="init/media/admin_interface/logo/logo-64x64.png" /><br>

The module [be-wal](./) is the custom FaST deployment for Wallonia. It inherits services from the FaST [core](https://gitlab.com/fastplatform-be-wal/core) and implements some [custom](services/) services for Wallonia. It also reuses some add-on services.

## Services

- Custom
    - [iacs/opw](services/iacs/opw): IACS connector for the Organisme Payeur de Wallonie (OPW)
    - [idp/authentication](services/idp/authentication): Identity provider for the Federal Authentication Service (FAS/CSAM)
    - [regulatory/nitrate](services/regulatory/nitrate): Calculation of nitrogen limits and warnings based on Protect'eau

- Add-ons
    - [requaferti](https://gitlab.com/fastplatform-be-wal/requaferti): Fertilization algorithm (proxy to the Requasud API + soil data + cache)
    - [agromet](https://gitlab.com/fastplatform-be-wal/agromet): Weather forecasts (service + cache)
    - [sobloo](https://gitlab.com/fastplatform-be-wal/sobloo): RGB/NDVI tile generation (service + database + API gateway)

- Core
    - [core/api_gateway](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/api_gateway): GraphQL API gateway
    - [core/event/farm](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/event/farm): Management of events related to farm structure (plots, soil estimates, etc)
    - [core/event/messaging](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/event/messaging): Management of messaging events (push/email notifications, etc)
    - [core/mapproxy](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/mapproxy): Map raster tiles proxying service + dynamic configuration
    - [core/mobile/push-notification](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/mobile/push-notification): Push notifications for Apple and Google
    - [core/web/backend](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/web/backend): Administration Portal
    - [core/web/media](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/web/media): Authorization proxy for media files (eg. photos)
    - [core/web/pdf_generator](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/web/pdf_generator): PDF generation for fertilization plan exports
    - [core/web/vector_tiles](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/web/vector_tiles): Map vector tiles generation, based on database data (eg. NVZ, Natura2000, hydro, etc)

## Deployment

The ```.gitlab-ci.yml``` file includes jobs for deploying the FaST services on the ```staging``` and ```beta``` environments:
* Commits from the ```master``` branch can be deployed on the ```staging``` environment (see ```deploy:staging``` job)
* Commits with an associated Git tag can be deployed on the ```beta``` environment (see ```deploy:beta``` job)

The deployment settings for each environment are defined in [tools/ci/envs](tools/ci/envs). Since these settings contain sensitive information such as passwords, all files are encrypted. Please refer to the [Encryption of sensitive information](#encryption-of-sensitive-information) section to decrypt them locally. 

## Encryption of sensitive information

Files with sensitive content are encrypted with [git-crypt](https://github.com/AGWA/git-crypt) and an asymmetric key.


The base64 encoded asymetric key can be retrieved from the settings of the [CI/CD variables](https://gitlab.com/fastplatform-be-wal/be-wal/-/settings/ci_cd) (CI_GIT_CRYPT_KEY) or ask your local administrator if you don't have enough permissions.

The files to be encrypted are declared in [.gitattributes](.gitattributes).

Follow the steps below to decrypt the files from this repository:

1. Install the version ```0.6.0``` of [git-crypt](https://github.com/AGWA/git-crypt)
2. Unlock the repository with the base64 encoded asymetric key
```bash
echo -n '<the base64 encoded asymetric key>' | base64 --decode | git crypt unlock -
```