# `be-wal/regulatory/nitrate`: Nitrogen limitations for Wallonia, based on protecteau.be

- [`be-wal/regulatory/nitrate`: Nitrogen limitations for Wallonia, based on protecteau.be](#be-walregulatorynitrate-nitrogen-limitations-for-wallonia-based-on-protecteaube)
  - [Architecture](#architecture)
  - [GraphQL server](#graphql-server)
  - [Data model](#data-model)
  - [Administration interface](#administration-interface)
  - [Localization](#localization)
  - [Computation](#computation)
    - [inputs](#inputs)
      - [from backend](#from-backend)
      - [from user_input (params node)](#from-user_input-params-node)
    - [mappings](#mappings)
    - [outputs](#outputs)
      - [if **plant_species_id** not found for plot](#if-plant_species_id-not-found-for-plot)
      - [if FERTILIZER_PARAM missing in user_inputs (params node)](#if-fertilizer_param-missing-in-user_inputs-params-node)
      - [for every combination of `is_in_nvz`/not and `crop_designation` found/not](#for-every-combination-of-is_in_nvznot-and-crop_designation-foundnot)
  - [Environment variables](#environment-variables)
  - [Development](#development)
    - [Launch core/event/farm](#launch-coreeventfarm)
    - [Launch regulatory/nitrate service](#launch-regulatorynitrate-service)
    - [Management commands](#management-commands)
  - [Running the tests](#running-the-tests)
  - [Crop designations / plant species mappings](#crop-designations--plant-species-mappings)
  - [Test queries for dev](#test-queries-for-dev)
    - [Query](#query)
    - [Variables](#variables)

In addition to the original algorithm, the FaST port provides:
- a GraphQL API to query the base data and to run nitrogen limitations
- a web administration interface to configure all the parameters of the algorithm, in real-time
- tests

## Architecture

The service is entirely built in [Python](https://www.python.org/) and comprises the following components:
- a web server based on the [Django](https://www.djangoproject.com/) framework that provides the GraphQL API (using [Graphene](https://graphene-python.org/)) and the web administration interface (using Django Admin)
- a [Postgres](https://www.postgresql.org/) database that stores the base data and configuration

Note: the web server is not designed to run without the database.

```plantuml
component "Hasura" as api_gateway <<Api gateway>>

folder "service" as ms1 {
 component "FastAPI" as core_event_farm <<""event / farm"">>
}

folder "service" as ms2 {
 frame "Administration interface" as admin_interface #lightgreen
 component "Django" as regulatory_nitrate_service <<""regulatory / nitrate"">> #yellow 
 
 database "postgres" as regulatory_nitrate_db <<""regulatory/nitrate"">>
  regulatory_nitrate_service -down-> regulatory_nitrate_db
}

folder "service" as ms3 {
 component "Backend" as core_backend <<""core / backend"">>
}

component "Mobile app" as mobile_app <<""mobile app"">> #lightblue
mobile_app -down->api_gateway: graphql\n""nitrogen_limitation""

api_gateway -down->regulatory_nitrate_service:remote shema
core_event_farm <-right->regulatory_nitrate_service:via Hasura\n""is in NVZ ?""
regulatory_nitrate_service<-right->core_backend:via Hasura\n""plot details""
admin_interface <-down->regulatory_nitrate_service
```

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:

```graphql
query {
    nitrogen_limitation
}

query {
    nitrogen_limitation__healthz
}

# The service does not expose any mutation or subscription.
```

The schemas are self documenting and we encourage the reader to introspect it to learn more about each node and its associated fields and relations. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools.


## Data model
```plantuml
component "Regulatory Nitrate" #lightyellow {
  entity CropDesignation {
      <i>Maximum organic and mineral
      <i>limit for various types of crops
      --
      * id
      --
      name 
      max_kg_organic_n_per_ha_per_year 
      organic_n_comment
      organic_n_comment_de
      max_kg_organic_plus_mineral_n_per_ha_per_year
      organic_plus_mineral_n_comment
      organic_plus_mineral_n_comment_de
  }

  entity PlantSpeciesToCropDesignation {
      <i>The mapping between plant species
      <i>in the IACS nomenclature
      <i>and the crop designation in service
      --
      * id
      --
      plant_species_id
      crop_designation
  }

  CropDesignation --o{ PlantSpeciesToCropDesignation

  entity Fertilizer {
      <i>The type of fertilizer 
      <i>used on the plot
      --
      * id
      --
      label
      label_de
  }

  entity SpreadingRequirement {
      <i>The type of requirements
      <i>to follow when spreading
      --
      * id
      --
      label 
      label_de 
  }

  entity SpreadingPeriod {
      <i>The periods for spreading
      --
      * id
      --
      crop_designation
      fertilizer
      nvz_specific
      display_order
      description
      description_de
      start_at
      end_at
      start_at_de
      end_at_de
  }

  CropDesignation --o{ SpreadingPeriod
  Fertilizer --o{ SpreadingPeriod

  entity SpreadingCondition {
      <i>The requirements to follow
      <i>per fertilizer
      --
      * id
      --
      spreading_requirement
      fertilizer
      content
      content_de 
  }

  Fertilizer --o{ SpreadingCondition
  SpreadingRequirement --o{ SpreadingCondition

  entity Warning {
      <i>The warnings to be added
      <i>to the limits computed
      --
      * id
      --
      title
      title_de
      content
      content_de
  }
}

component "Backend" {
  entity PlantSpecies {
      <i>The fast plant species
      <i>in the IACS nomenclature
      --
      * id
      --
      name
  }
}

PlantSpecies --o{ PlantSpeciesToCropDesignation
```


## Administration interface

<img src="docs/images/admin.png" width="800"/>

The service exposes an administration interface on the `/admin` endpoint, which allows the administrator to manage the base data tables of the algorithm:
- Plant species to crop designations (mapping)
- Crop designations
- Fertilizers
- Spreading conditions
- Spreading periods
- Spreading requirements
- Warnings
- Config

## Localization

The administration interface is localized in the following languages:
- Bulgarian / Български
- Greek / Ελληνική
- English
- Spanish / Castellano
- Estonian / Eesti
- French / Français
- Italian / Italiano
- Romanian / Română
- Slovak / Slovenčina

Note that the content of the configuration tables (e.g. the crop profiles) is not localized (it is in the language it was loaded in).


## Computation
https://protecteau.be/fr/nitrate/agriculteurs/legislations/conditionnalite/aspects-pgda

### inputs

#### from backend 

From `plot_id`, get:

- is_in_nvz
- plant_species_id

#### from user_input (params node)
- FERTILIZER_PARAM

### mappings
- plant_species_id -> [crop_designation] (can be more than one mapping found !)


### outputs

#### if **plant_species_id** not found for plot

```json
{
  "data": {
    "nitrogen_limitation": {
      "nitrogen_limits": null,
      "warnings": null,
      "params": null,
      "missing_base_params": [plot_plant_species_missing]
    }
  }
}
```

QUERY TESTS: 
- `01_de_plant_species_missing`
- `07_plant_species_missing`

#### if FERTILIZER_PARAM missing in user_inputs (params node)

```json
{
  "data": {
    "nitrogen_limitation": {
      "nitrogen_limits": null,
      "warnings": null,
      "params": [
        {
          "id": "FERTILIZER_PARAM",
          "label": "Type de fertilisant utilisé",
          "choices": [
            {
              "id": "FERTILISANT_MINERAL",
              "label": "Azote minéral"
            },
            {
              "id": "FERTILISANT_ORGANIQUE_LIQUIDE",
              "label": "Fumier mou, lisier, purin, fientes et fumier de volailles"
            },
            {
              "id": "FERTILISANT_ORGANIQUE_SOLIDE",
              "label": "Fumier et compost"
            }
          ]
          "value": null # no value chosen yet by user
        }
      ],
      "missing_base_params": []
    }
  }
}
```

QUERY TESTS: 
- `02_de_missing_param`
- `08_missing_param`

#### for every combination of `is_in_nvz`/not and `crop_designation` found/not

```json
{
  "data": {
    "nitrogen_limitation": {
      "nitrogen_limits": [
        {
            "id": "902c5de2dcf6f94f6f5b9ee530e4787ed8b47990",
            "value": float,
            "unit": "kg N/ha/an",
            "comment": "En zone vulnérable, la quantité moyenne d'azote organique apportée par hectare de l'ensemble de l'exploitation (cultures et prairies) ne peut dépasser 170 kg/ha."
        }
      ],
      "warnings": [
        {
          "id": "902c5de2dcf6f94f6f5b9ee530e4787ed8b47990",
          "title": null,
          "content": "Moins de 6 mètres de tout point d'eau: Interdit",
          "url": null
        },
        {
          "id": "eba4e4cc786acde8f5b9ee530e4787ed8b94f6f5",
          "title": null,
          "content": "Sol inondé ou recouvert de neige: Interdit",
          "url": null
        },
        {
          "id": "902c5de2dcf6f94f6f5b9ee530e4787ed8b47990",
          "title": null,
          "content": "Avant, pendant, ou après une légumineuse (sauf si conseil ferti): Interdit",
          "url": null
        },
        {
          "id": "902c5de2dcf6f94f6f5b9ee530e4787ed8b47990",
          "title": null,
          "content": "Sol gelé: Interdit",
          "url": null
        },
        {
          "id": "eba4e4cc786acde8f5b9ee530e4787ed8b94f6f5",
          "title": null,
          "content": "Sol gelé en zone vulnérable: Interdit",
          "url": null
        },
        {
          "id": "eba4e4cc786acde8f5b9eeeba4e4cc786acde8",
          "title": null,
          "content": "Sol nu: Autorisé",
          "url": null
        },
        {
          "id": "PLOT_7_WARNING_7",
          "title": null,
          "content": "Culture avec pente de plus de 15%: Interdit sur la partie réellement en pente",
          "url": null
        }
        …
      ],
      "params": [
        {
          "id": "FERTILIZER_PARAM",
          "label": "Type de fertilisant utilisé",
          "choices": [
            {
              "id": "FERTILISANT_MINERAL",
              "label": "Azote minéral"
            },
            {
              "id": "FERTILISANT_ORGANIQUE_LIQUIDE",
              "label": "Fumier mou, lisier, purin, fientes et fumier de volailles"
            },
            {
              "id": "FERTILISANT_ORGANIQUE_SOLIDE",
              "label": "Fumier et compost"
            }
          ]
          "value": "FERTILISANT_MINERAL" # value chosen by user
        }
      ],
      "missing_base_params": null
    }
  }
}
```

QUERY TESTS: 
- `03_de_in_nvz_with_crop_designation`
- `04_de_outside_nvz_with_crop_designation`
- `05_de_outside_nvz_no_crop_designation`
- `06_de_in_nvz_no_crop_designation`
- `09_in_nvz_with_crop_designation`
- `10_outside_nvz_with_crop_designation`
- `11_outside_nvz_no_crop_designation`
- `12_in_nvz_no_crop_designation`

## Environment variables


Below are the available environment variables for the web server:
- `DJANGO_SECRET_KEY`: See [SECRET_KEY](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-SECRET_KEY). No default value.
- `DEBUG`: See [DEBUG](https://docs.djangoproject.com/en/3.2/ref/settings/#debug). Defaults to `True`.
- `DOMAIN_NAME`: The domain on which the service is running. Ignored if `DEBUG` is `True`. No default value.
- `DJANGO_ALLOWED_HOSTS`: See [ALLOWED_HOSTS](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-ALLOWED_HOSTS)
- `DJANGO_SECURE_HSTS_SECONDS`: See [SECURE_HSTS_SECONDS](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-SECURE_HSTS_SECONDS)
- `DJANGO_CSRF_COOKIE_SECURE`: See [CSRF_COOKIE_SECURE](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-CSRF_COOKIE_SECURE)

- `POSTGRES_DATABASE`: See [NAME](https://docs.djangoproject.com/en/3.2/ref/settings/#name). No default value.
- `POSTGRES_USER`: See [USER](https://docs.djangoproject.com/en/3.2/ref/settings/#user). No default value.
- `POSTGRES_PASSWORD`: See [PASSWORD](https://docs.djangoproject.com/en/3.2/ref/settings/#password). No default value.
- `POSTGRES_HOST`: See [HOST](https://docs.djangoproject.com/en/3.2/ref/settings/#host). No default value.
- `POSTGRES_PORT`: See [PORT](https://docs.djangoproject.com/en/3.2/ref/settings/#port). No default value.
- `POSTGRES_CONN_MAX_AGE`: See [CONN_MAX_AGE](https://docs.djangoproject.com/en/3.2/ref/settings/#conn-max-age). Defaults to `0`.
- `DISABLE_SERVER_SIDE_CURSORS`: See [DISABLE_SERVER_SIDE_CURSORS](https://docs.djangoproject.com/en/4.0/ref/settings/#disable-server-side-cursors). Defaults to `True`.

- `LANGUAGE_CODE`: See [LANGUAGE_CODE](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-LANGUAGE_CODE). Defaults to `en`.
- `TIME_ZONE`: See [TIME_ZONE](https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-TIME_ZONE). Defaults to `UTC`.

## Development 

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

The present service requires `core-event-farm` service to be up and running, and the corresponding remote schema to be mounted in main Hasura.

### Launch core/event/farm
```bash
cd …/core/services/event/farm

# launch service
make start
```

Make sure your local version of `core/services/api_gateway/fastplatform/metadata/remote_schemas.yaml` contains the following:
```yaml
- name: core-event-farm
  definition:
    url_from_env: REMOTE_SCHEMA_URL_CORE_EVENT_FARM
    timeout_seconds: 60
```

If needed, apply metadata:
```bash
cd …/core/services/api_gateway/fastplatform

make apply
```

Open Hasura UI, make sure the `core-event-farm` remote schema is visible on page `[hasura]/console/remote-schemas/manage/schemas`.

Troubleshooting:

If `core-event-farm` does not appear in list, try the following:
- Simply refresh page
- If still not working, navigate to `[hasura]/console/settings/metadata-actions`, check `Reload all remote schemas`, and click `Reload`.


### Launch regulatory/nitrate service
```bash
# cd back to this service:
cd …/custom/[region]/services/regulatory/nitrate
```

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the Postgres server:
```bash
make start-postgres
# make stop-postgres (to stop the server)
```

Apply migrations:
```bash
make migrate
```

If you need to clear existing data:
```bash
make clear-data
```

Fill in the database with the base Nitrogen Limitation data:
```bash
make init-data
```

Start the development server with auto-reload enabled:
```bash
make start
# Or with certificates, on https:
make starts
```

The GraphQL endpoint is now available at `http://localhost:8000/graphql` and the administration interface is available at `http://localhost:8000/admin`.

### Management commands

The following management commands are available from the `Makefile`.

- Django targets:
    - `make start`: Start the development server with auto-reload enabled.
    - `make starts`: Start the development server `https` on with auto-reload enabled (assumes that you have the relevant certificates in the `/certificates` directory).
    - `make migrations`: Generate Django migrations
    - `make migrate`: Apply Django migrations
    - `make collectstatic`: Collect static files in the `/static` directory

- Postgres targets:
    - `make start-postgres`: Start the local Postgres server
    - `make stop-postgres`: Stop the local Postgres server
    - `make reset-postgres`: Delete the data of the local Postgres server

- Internationalisation targets:
    - `make messages`: Generate localized files (`.po`) for defined locales and extract untranslated strings
    - `make messages-csv`: Generate localized files (`.csv`) for defined locales (you need to have the `po2csv` utility installed, from the [Translate Toolkit](http://docs.translatehouse.org/projects/translate-toolkit/))
    - `make csv-messages`: Generate `.po` files from `.csv` for defined locales
    (you need to have the `csv2po` utility installed, from the [Translate Toolkit](http://docs.translatehouse.org/projects/translate-toolkit/))
    - `make compile-messages`: Compile `django.po` into `django.mo`

- Data loading targets:
    - `make create-super-user`: Create a super user with the given username "admin" and password "azerty".
    - `make init-data`:  Load seed data in the database
    - `make clear-data`: Clear data from the database

- Testing targets:
    - `make test`: Run the test suite
	
Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).

## Running the tests

You can find the tests in this [folder](nitrogen_limitation/tests/).

Before running the tests, clearing and initializing the database is required. From core/services/backend:
```sh
make clear-region clear-region-external init-region init-region-external REGION=be-wal  
```

In addition, Graphql queries require: 

- a running instance of `core-event-farm` exposed on localhost, port 7010
- `core-event-farm` added in remote schema of `hasura-fastplatform`
- metadata of `hasura-fastplatform` applied
- a running instance of `hasura-fastplatform`


Once your setup is deployed, run:
```bash
make test
```

## Crop designations / plant species mappings

For the proper functioning of the algorithm, plant species of the FaST database need to be mapped to crop designations.


```plantuml
package "FaST Data Model" as fast <<Rectangle>> {
    class PlantSpeciesGroup {
        id
        name
        …
    }

    class PlantSpecies {
        id
        name
        group_id
        …
    }

    class PlantVariety {
        id
        name
        plant_species_id
        …
    }

    class PlotPlantVariety {
        id
        plant_variety_id
        plot_id
        crop_yield
        …
    }

    class Plot {
        id
        authority_id
        name
        …
    }

    PlantSpecies::group_id -up-> PlantSpeciesGroup::id
    PlantVariety::plant_species_id -up-> PlantSpecies::id
    PlotPlantVariety::plant_variety_id -left-> PlantVariety::id
    PlotPlantVariety::plot_id -right-> Plot::id
}

package "Nitrogen Limitation" as nl <<Rectangle>> {
    class CropDesignation {
        id
        name
        max_kg_organic_n_per_ha_per_year
        organic_n_comment
        max_kg_organic_plus_mineral_n_per_ha_per_year
        organic_plus_mineral_n_comment
    }

    class PlantSpeciesToCropDesignation {
        id
        crop_designation_id
        plant_species_id
    }
}

PlantSpeciesToCropDesignation::crop_designation_id -right-> CropDesignation::id
PlantSpeciesToCropDesignation::plant_species_id -right-> PlantSpecies::id
```


## Test queries for dev

Copy-paste in Altair, or any graphql client

### Query
```graphql
query health {
  nitrogen_limitation__healthz
}

query nitrogen_limitation($input: nitrogen_limitation_input!) {
  nitrogen_limitation(input: $input) {
    nitrogen_limits{
      value
      unit
      comment
    }
    warnings {
      title
      content
      url
    }
    missing_params {
      id
      label
      choices {
        id
        label
      }
    }
    missing_base_params
  }
}
```

### Variables

To return missing params
```json
{
  "input": {
    "plot_id": 1,
    "params": "{}"
  }
}
```

To return data complete
requires:
- a plot with id `1`
- with a plant_species associated to it
- this plant_species_id needs a mapping to crop_designation

```json
{
  "input": {
    "plot_id": 1,
    "params": "{\"FERTILIZER_PARAM\": \"FERTILISANT_ORGANIQUE_SOLIDE\"}"
  }
}
```
