from collections import OrderedDict

from import_export import resources
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget


class ModelDeclarativeMetaclassWithIds(resources.ModelDeclarativeMetaclass):
    def __new__(cls, name, bases, attrs):
        """We override the Resource metaclass to impose that the field names
        match the column names in Postgres (for foreign keys). This allows us
        to use the same files as we do for PG COPY imports.
        """
        new_class = super().__new__(cls, name, bases, attrs)
        # print(new_class._meta.model)
        new_fields = []
        for field_name, field in new_class.fields.items():
            if isinstance(field.widget, ForeignKeyWidget):
                field.column_name = field_name + "_id"
                new_fields += [(field_name + "_id", field)]
            elif isinstance(field.widget, ManyToManyWidget):
                field.column_name = field_name + "_ids"
                new_fields += [(field_name + "_ids", field)]
            else:
                new_fields += [(field_name, field)]
        new_class.fields = OrderedDict(new_fields)
        return new_class


class ModelResourceWithColumnIds(
    resources.ModelResource, metaclass=ModelDeclarativeMetaclassWithIds
):
    skip_unchanged = True
    report_skipped = True
