import logging
import json
from functools import reduce
import hashlib

import graphene

from constance import config
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from gql import gql

from nitrogen_limitation.schema.types import (
    NitrogenLimitationInputType,
    NitrogenLimitationOutputType,
    ParamType,
    ParamChoiceType,
    MissingBaseParamType,
    WarningType,
)

from nitrogen_limitation.errors import (
    GraphQLServiceError,
    GraphQLPlotNotFoundError,
)

from db.graphql_clients import FastplatformClient

from nitrogen_limitation.models import (
    CropDesignation,
    Fertilizer,
    SpreadingPeriod,
    SpreadingCondition,
    Warning,
)


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

FERTILIZER_PARAM = "FERTILIZER_PARAM"
PARAMS = [FERTILIZER_PARAM]
ACCEPTED_LANGUAGES = [settings.LANGUAGE_CODE, "de"]


class NitrogenLimitationQuery(graphene.ObjectType):
    nitrogen_limitation = graphene.Field(
        NitrogenLimitationOutputType,
        input=graphene.Argument(NitrogenLimitationInputType, required=True),
    )

    def resolve_nitrogen_limitation(self, info, input):
        try:
            logger.info("----------")
            language = info.context.META.get("HTTP_X_HASURA_LANGUAGE")
            logger.info("Language from request info: %s", language)
        except Exception as ex:
            logger.exception(
                "Could not retrieve language from info: %s, will resolve with settings.LANGUAGE_CODE: %s",
                info,
                settings.LANGUAGE_CODE,
            )

        if language == None:
            language = settings.LANGUAGE_CODE

        if not language in ACCEPTED_LANGUAGES:
            logger.info(
                "Not processing language: %s, will resolve with settings.LANGUAGE_CODE: %s",
                language,
                settings.LANGUAGE_CODE,
            )
            language = settings.LANGUAGE_CODE

        logger.info("Resolving with language: %s", language)
        with translation.override(language):
            return NitrogenLimitationQuery.resolve(info, input, language)

    @staticmethod
    def resolve(info, input, language):
        plot_id = input.plot_id

        user_id, role = NitrogenLimitationQuery.get_x_hasura_user_id_and_role(info)
        logger.info(
            "resolve_nitrogen_limitation initiated for plot_id, user, role, language: %s, %s, %s",
            plot_id,
            user_id,
            role,
        )

        fastplatform_client = NitrogenLimitationQuery.get_fastplatform_client(
            user_id, role, FastplatformClient
        )

        base_params = NitrogenLimitationQuery.get_base_params(
            plot_id, fastplatform_client
        )

        if "plant_species_id" not in base_params or not base_params["plant_species_id"]:
            # Notify user they need to select a crop for the plot
            logger.info("plant_species_id missing, prompt user")
            return NitrogenLimitationOutputType(
                missing_base_params=[MissingBaseParamType.plot_plant_species_missing],
            )

        logger.info("base_params: %s", base_params)

        params_node, params = NitrogenLimitationQuery.build_params_node(input, language)

        output = NitrogenLimitationOutputType(
            params=params_node, missing_base_params=[]
        )

        if not all(param.value is not None for param in params_node):
            logger.info("Some params missing, ask user")
            return output

        logger.info("params found: %s", params)

        crop_designation = NitrogenLimitationQuery.get_crop_designation(
            base_params["plant_species_id"]
        )

        logger.info(
            "crop_designation found"
            if crop_designation
            else "crop designation not found"
        )

        return NitrogenLimitationQuery.compute(
            base_params, params, crop_designation, plot_id, params_node, language
        )

    @staticmethod
    def get_x_hasura_user_id_and_role(info):
        """
        This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header or role
        """
        logger.info("get_x_hasura_user_id_and_role")
        try:
            user_id = info.context.META.get("HTTP_X_HASURA_USER_ID")
            role = info.context.META.get("HTTP_X_HASURA_ROLE")

            if not user_id or not role:
                logger.warning(
                    "get_x_hasura_user_id_and_role, no user_id / role: %s / %s",
                    user_id is not None,
                    role is not None,
                )
                raise GraphQLServiceError()

            return user_id, role
        except AttributeError as ex:
            logger.exception(ex)
            raise GraphQLServiceError() from ex

    @staticmethod
    def build_params_node(input, language):
        logger.info("build_params_node with input params: %s", input.params)
        if input.params is None:
            logger.exception("No input.params in query.")
            raise GraphQLServiceError()

        try:
            input_params = json.loads(input.params)
        except Exception as ex:
            logger.exception("Could not parse parameters: %s", input.params)
            raise GraphQLServiceError()

        fertilizers = list(Fertilizer.objects.all().values("id", "label", "label_de"))

        value = None

        if FERTILIZER_PARAM in input_params:
            value = input_params[FERTILIZER_PARAM]
            logger.info("%s from user input to %s", FERTILIZER_PARAM, value)

        if value == None:
            logger.info("Param %s is missing, which will prompt user", FERTILIZER_PARAM)

        question_label = (
            config.FERTILIZER_PARAM_QUESTION_DE
            if language == "de"
            else config.FERTILIZER_PARAM_QUESTION_FR
        )

        choice_label_column = "label_de" if language == "de" else "label"

        params = [
            ParamType(
                id=FERTILIZER_PARAM,
                label=question_label,
                choices=[
                    ParamChoiceType(
                        id=str(fertilizer["id"]).upper(),
                        label=fertilizer[choice_label_column],
                    )
                    for fertilizer in fertilizers
                ],
                value=value,
            ),
        ]

        logger.info("params: %s", params)
        return params, {FERTILIZER_PARAM: value}

    @staticmethod
    def get_fastplatform_client(user_id, role, client_class):
        """
        fastplatform_client injected in get_plot_plant_species_id for cleaner testing
        """
        logger.info("get_fastplatform_client")
        additional_headers = {
            "X-Hasura-User-Id": user_id,
            "X-Hasura-Role": role,
        }

        return client_class(additional_headers)

    @staticmethod
    def get_base_params(plot_id, fastplatform_client):
        logger.info("get_base_params for plot %s", plot_id)
        query = (
            settings.BASE_DIR / "nitrogen_limitation/graphql/query_plot_params.graphql"
        ).read_text()
        variables = {"plot_id": int(plot_id)}

        try:
            response = fastplatform_client.execute(gql(query), variables)
        except Exception as ex:
            logger.exception("Could not perform get_base_params query")
            raise GraphQLServiceError()

        if (
            response is None
            or not isinstance(response, dict)
            or not "plot" in response
            or not isinstance(response["plot"], list)
        ):
            logger.warning("query_plot_params response empty")
            raise GraphQLServiceError()

        if len(response["plot"]) == 0:
            logger.warning("No plot found for id %s", plot_id)
            raise GraphQLPlotNotFoundError(plot_id)

        if len(response["plot"]) > 1:
            logger.warning(
                "More than one plot_plant_variety found, only processing first"
            )

        # Get plant_species_id
        try:
            # Only ONE plot_plant_variety per plot
            plot_plant_variety = response["plot"][0]["plot_plant_variety"]

            base_params = {}
            base_params["plant_species_id"] = plot_plant_variety["plant_variety"][
                "plant_species"
            ]["id"]

            if not base_params["plant_species_id"]:
                raise AttributeError

        except Exception as ex:
            # missing_base_param will be added in main function
            logger.warning("No plant species found on plot: %s", plot_id)

        # Get is in management regulation zone
        base_params["is_in_management_restriction_or_regulation_zone"] = (
            response["computed_constraints_management_restriction_or_regulation_zone"]
            is not None
        )

        return base_params

    @staticmethod
    def get_crop_designation(plot_plant_species_id):
        logger.info("get_crop_designation %s", plot_plant_species_id)
        try:
            return CropDesignation.objects.filter(
                plant_species_crop_designation__plant_species_id=plot_plant_species_id
            ).get()

        except CropDesignation.DoesNotExist as ex:
            logger.warning("CropDesignation not found: %s", plot_plant_species_id)
            return None

    @staticmethod
    def compute(
        base_params,
        params,
        crop_designation: CropDesignation,
        plot_id,
        params_node,
        language,
    ):

        language_is_de = language == "de"
        logger.info("compute")
        in_nvz = base_params["is_in_management_restriction_or_regulation_zone"]
        fertilizer_id = str(params[FERTILIZER_PARAM]).lower()

        logger.info(
            "in_nvz: %s, crop_designation found: %s, fertilizer_id: %s, language: %s",
            in_nvz,
            crop_designation is not None,
            fertilizer_id,
            language,
        )

        nitrogen_limits = []

        # ---------------------------------
        # 0. Add regulatory warnings if any
        warnings = [
            WarningType(
                id=hashlib.sha1(
                    str.encode(f"{plot_id}-{w.id}-{w.title}-{w.content}-{w.url}")
                ).hexdigest(),
                title=w.title_de if language_is_de else w.title,
                content=w.content if language_is_de else w.content,
                url=w.url if language_is_de else w.url,
            )
            for w in Warning.objects.all()
        ]

        # --------------------------
        # 1. Compute nitrogen limits
        logger.info("1. Compute nitrogen limits")
        if in_nvz:
            nvz_limit_comment = (
                config.ORGANIC_N_COMMENT_IN_NVZ_DE
                if language == "de"
                else config.ORGANIC_N_COMMENT_IN_NVZ_FR
            )
            nitrogen_limits.append(
                {
                    "id": hashlib.sha1(
                        str.encode(
                            f"nitrogen_limit_in_nvz_{plot_id}-{config.MAX_KG_ORGANIC_N_PER_HA_PER_YEAR_IN_NVZ}-{nvz_limit_comment}"
                        )
                    ).hexdigest(),
                    "value": config.MAX_KG_ORGANIC_N_PER_HA_PER_YEAR_IN_NVZ,
                    "unit": str(_("kg N/ha/year")),
                    "comment": nvz_limit_comment,
                }
            )

        elif crop_designation is not None:
            if crop_designation.max_kg_organic_n_per_ha_per_year is not None:
                nitrogen_limits.append(
                    {
                        "id": hashlib.sha1(
                            str.encode(
                                f"nitrogen_limit_organic_{plot_id}-{crop_designation.max_kg_organic_n_per_ha_per_year}-{crop_designation.organic_n_comment}"
                            )
                        ).hexdigest(),
                        "value": crop_designation.max_kg_organic_n_per_ha_per_year,
                        "unit": str(_("kg N/ha/year")),
                        "comment": crop_designation.organic_n_comment_de
                        if language_is_de
                        else crop_designation.organic_n_comment,
                    }
                )

            if (
                crop_designation.max_kg_organic_plus_mineral_n_per_ha_per_year
                is not None
            ):
                nitrogen_limits.append(
                    {
                        "id": hashlib.sha1(
                            str.encode(
                                f"nitrogen_limit_mineral_plus_organic_{plot_id}-{crop_designation.max_kg_organic_plus_mineral_n_per_ha_per_year}-{crop_designation.organic_plus_mineral_n_comment}"
                            )
                        ).hexdigest(),
                        "value": crop_designation.max_kg_organic_plus_mineral_n_per_ha_per_year,
                        "unit": str(_("kg N/ha/year")),
                        "comment": crop_designation.organic_plus_mineral_n_comment_de
                        if language_is_de
                        else crop_designation.organic_plus_mineral_n_comment,
                    }
                )

        # -------------------------------
        # 2. Compute spreading conditions

        logger.info("2. Compute spreading conditions")

        spreading_conditions = SpreadingCondition.objects.filter(
            fertilizer_id=fertilizer_id
        ).select_related("spreading_requirement")

        for sc in spreading_conditions:
            id = hashlib.sha1(
                str.encode(
                    f"spreading_condition_{plot_id}-{sc.id}-{sc.spreading_requirement.label}-{sc.content}-{config.SPREADING_CONDITIONS_URL}"
                )
            ).hexdigest()

            sc_label = (
                sc.spreading_requirement.label_de
                if language_is_de
                else sc.spreading_requirement.label
            )
            sc_content = sc.content_de if language_is_de else sc.content

            content = f"{sc_label}: {sc_content}"

            warnings.append(
                {"id": id, "content": content, "url": config.SPREADING_CONDITIONS_URL}
            )

        logger.info(
            "%s warnings with spreading_conditions",
            len(warnings),
        )

        # ----------------------------
        # 3. Compute spreading periods

        logger.info("3. Compute spreading periods")

        # Spreading periods can only be computed with a crop_designation set
        # Return now if crop_designation None
        if crop_designation is None:
            logger.info(
                "No spreading periods returned because crop designation is None"
            )
            return NitrogenLimitationOutputType(
                nitrogen_limits=nitrogen_limits,
                warnings=warnings,
            )

        spreading_periods = SpreadingPeriod.objects.filter(
            crop_designation_id=crop_designation.id,
            fertilizer_id=fertilizer_id,
        ).order_by("display_order")

        # If spreading_periods returned by query contain False *and* True values for nvz_specific,
        # filter out values that do not correspond to the in_nvz parameter of plot
        if reduce(lambda x, y: x ^ y, [sp.nvz_specific for sp in spreading_periods]):
            logger.info("filtering values from spreading_periods according to in_nvz")
            spreading_periods = [
                sp for sp in spreading_periods if sp.nvz_specific == in_nvz
            ]

        for sp in spreading_periods:
            id = hashlib.sha1(
                str.encode(
                    f"spreading_period_{plot_id}-{sp.id}-{sp.description}-{sp.start_at}-{sp.end_at}-{config.SPREADING_PERIODS_URL}"
                )
            ).hexdigest()

            sp_description = sp.description_de if language_is_de else sp.description
            sp_start_at = sp.start_at_de if language_is_de else sp.start_at
            sp_end_at = sp.end_at_de if language_is_de else sp.end_at

            content = (
                f"{sp_description} zwischen dem {sp_start_at} und dem {sp_end_at}."
                if language_is_de
                else f"{sp_description} entre le {sp_start_at} et le {sp_end_at}."
            )

            warnings.append(
                {"id": id, "content": content, "url": config.SPREADING_PERIODS_URL}
            )

        logger.info(
            "%s warnings with spreading_periods",
            len(warnings),
        )

        # ---------------------------------
        return NitrogenLimitationOutputType(
            nitrogen_limits=nitrogen_limits,
            warnings=warnings,
            params=params_node,
            missing_base_params=[],
        )
