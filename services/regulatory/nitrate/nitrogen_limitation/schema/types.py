import graphene
from graphene.types.generic import GenericScalar


class NitrogenLimitationInputType(graphene.InputObjectType):
    plot_id = graphene.ID(required=True)
    params = GenericScalar(required=True)

    class Meta:
        name = "nitrogen_limitation_input"


class ParamChoiceType(graphene.ObjectType):
    id = graphene.String(required=True)
    label = graphene.String(required=True)

    class Meta:
        name = "nitrogen_limitation_param_choice"


class ParamType(graphene.ObjectType):
    id = graphene.String(required=True)
    label = graphene.String(required=True)
    choices = graphene.List(ParamChoiceType, required=True)
    value = graphene.String(required=False)

    class Meta:
        name = "nitrogen_limitation_missing_param"


class NitrogenLimitType(graphene.ObjectType):
    id = graphene.String(required=True)
    value = graphene.Float(required=True)
    unit = graphene.String()
    comment = graphene.String()

    class Meta:
        name = "nitrogen_limitation_nitrogen_limit"


class MissingBaseParamType(graphene.Enum):
    plot_plant_species_missing = "plot_plant_species_missing"
    plot_crop_yield_missing = "plot_crop_yield_missing"

    class Meta:
        name = "nitrogen_limitation_missing_base_param"


class WarningType(graphene.ObjectType):
    id = graphene.String(required=True)
    title = graphene.String(required=False)
    content = graphene.String(required=True)
    url = graphene.String(required=False)

    class Meta:
        name = "nitrogen_limitation_warning"


class NitrogenLimitationOutputType(graphene.ObjectType):
    nitrogen_limits = graphene.List(NitrogenLimitType)
    warnings = graphene.List(WarningType)
    params = graphene.List(ParamType)
    missing_base_params = graphene.List(MissingBaseParamType)

    class Meta:
        name = "nitrogen_limitation_output"
