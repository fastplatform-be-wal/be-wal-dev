from graphql import GraphQLError


class GraphQLServiceError(GraphQLError):
    message = "SERVICE_ERROR"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLServiceError.message, *args, **kwargs)


class GraphQLPlotNotFoundError(GraphQLError):
    message = "PLOT_NOT_FOUND"

    def __init__(self, plot_id, *args, **kwargs):
        super().__init__(GraphQLPlotNotFoundError.message, *args, **kwargs)
        self.plot_id = plot_id
