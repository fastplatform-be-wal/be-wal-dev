from typing import OrderedDict
from django.utils.translation import ugettext_lazy as _


NITROGEN_LIMITATION_CONFIG = OrderedDict(
    [
        (
            "FERTILIZER_PARAM_QUESTION_FR",
            (
                "Type de fertilisant utilisé",
                _(
                    "The question to ask the french-speaking users to know what type of fertilizer they want to spread on their plot"
                ),
                str,
            ),
        ),
        (
            "FERTILIZER_PARAM_QUESTION_DE",
            (
                "Art des verwendeten Düngers",
                _(
                    "The question to ask the german-speaking users to know what type of fertilizer they want to spread on their plot"
                ),
                str,
            ),
        ),
        (
            "MAX_KG_ORGANIC_N_PER_HA_PER_YEAR_IN_NVZ",
            (
                170.0,
                _(
                    "The organic nitrogen limit per hectare per year in vulnerable zones"
                ),
                float,
            ),
        ),
        (
            "ORGANIC_N_COMMENT_IN_NVZ_FR",
            (
                "En zone vulnérable, la quantité moyenne d'azote organique apportée par hectare de l'ensemble de l'exploitation (cultures et prairies) ne peut dépasser 170 kg/ha.",
                _(
                    "Informational comment in french added to the organic nitrogen limit in vulnerable zone"
                ),
                str,
            ),
        ),
        (
            "ORGANIC_N_COMMENT_IN_NVZ_DE",
            (
                "In gefährdeten Gebieten darf die durchschnittliche Menge an organischem Stickstoff, die pro Hektar aus dem gesamten Betrieb (Kulturen und Grünland) eingebracht wird, 170 kg/ha nicht überschreiten.",
                _(
                    "Informational comment in german added to the organic nitrogen limit in vulnerable zone"
                ),
                str,
            ),
        ),
        (
            "SPREADING_PERIODS_URL",
            (
                "https://protecteau.be/fr/nitrate/agriculteurs/epandage#periodes",
                _("The url of the regulation for spreading periods"),
                str,
            ),
        ),
        (
            "SPREADING_CONDITIONS_URL",
            (
                "https://protecteau.be/fr/nitrate/agriculteurs/epandage#conditions",
                _("The url of the regulation for spreading conditions"),
                str,
            ),
        ),
    ]
)

NITROGEN_LIMITATION_CONFIG_FIELDSETS = OrderedDict(
    [
        (
            lambda: _("Algorithm constants"),
            (
                "FERTILIZER_PARAM_QUESTION_FR",
                "FERTILIZER_PARAM_QUESTION_DE",
                "MAX_KG_ORGANIC_N_PER_HA_PER_YEAR_IN_NVZ",
                "ORGANIC_N_COMMENT_IN_NVZ_FR",
                "ORGANIC_N_COMMENT_IN_NVZ_DE",
                "SPREADING_PERIODS_URL",
                "SPREADING_CONDITIONS_URL",
            ),
        ),
    ]
)
