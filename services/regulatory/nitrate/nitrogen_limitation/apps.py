from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class NitrogenLimitationConfig(AppConfig):
    name = "nitrogen_limitation"
    verbose_name = _("Nitrogen Limitation")
