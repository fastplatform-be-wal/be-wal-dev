from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from db.graphql_clients import FastplatformClient
from django.conf import settings

from gql import gql


class CropDesignation(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=255,
        verbose_name=_("identifier"),
        unique=True,
        db_index=True,
        help_text=_("Crop designation identifier"),
    )

    name = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        verbose_name=_("name"),
        unique=True,
        db_index=True,
        help_text=_("Crop designation name"),
    )

    max_kg_organic_n_per_ha_per_year = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("organic kg N/ha/year"),
        default=1,
    )

    organic_n_comment = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("organic comment in French"),
        help_text=_(
            """Informational comment added to the organic nitrogen limit in French"""
        ),
    )

    organic_n_comment_de = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("organic comment in German"),
        help_text=_(
            """Informational comment added to the organic nitrogen limit in German"""
        ),
    )

    max_kg_organic_plus_mineral_n_per_ha_per_year = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("organic + mineral kg N/ha/year"),
        default=1,
    )

    organic_plus_mineral_n_comment = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("organic + mineral comment in French"),
        help_text=_(
            """Informational comment added to the organic + mineral nitrogen limit in French"""
        ),
    )

    organic_plus_mineral_n_comment_de = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("organic + mineral comment in German"),
        help_text=_(
            """Informational comment added to the organic + mineral nitrogen limit in German"""
        ),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "crop_designation"
        verbose_name = _("crop designation")
        verbose_name_plural = _("crop designations")
        ordering = ["id"]


class PlantSpeciesToCropDesignation(models.Model):
    """Maps the relationship between a crop designation (in Nitrogen Limitation)
    and a plant species (in the IACS nomenclature of the target country)"""

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    plant_species_id = models.CharField(
        null=False,
        blank=False,
        max_length=8,
        verbose_name=_("plant species identifier"),
        unique=True,
        db_index=True,
        help_text=_(
            "Plant species identifier in the FaST database, usually corresponds to the IACS plant species identifier"
        ),
    )

    crop_designation = models.ForeignKey(
        to="nitrogen_limitation.CropDesignation",
        related_name="plant_species_crop_designation",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("crop designation"),
    )

    def __str__(self):
        return "{} --> {}".format(self.plant_species_id, self.crop_designation)

    def clean(self):
        super().clean()

        query = (
            settings.BASE_DIR
            / "nitrogen_limitation/graphql/query_plant_species_by_pk.graphql"
        ).read_text()
        variables = {"pk": self.plant_species_id}

        response = FastplatformClient().execute(gql(query), variables)

        if (
            not "plant_species_by_pk" in response
            or response["plant_species_by_pk"] is None
        ):
            raise ValidationError(
                _("Plant species identifier not found in IACS nomenclature:")
                + "%(value)s",
                code="invalid",
                params={"value": self.plant_species_id},
            )

    class Meta:
        db_table = "plant_species_to_crop_designation"
        verbose_name = _("plant species to crop designation")
        verbose_name_plural = _("plant species to crop designations")
        ordering = ["plant_species_id"]
        unique_together = (
            "plant_species_id",
            "crop_designation",
        )


class Fertilizer(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=255,
        verbose_name=_("identifier"),
        unique=True,
        db_index=True,
    )

    label = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        verbose_name=_("fertilizer french label"),
        unique=True,
        db_index=True,
    )

    label_de = models.CharField(
        null=True,
        blank=False,
        max_length=255,
        verbose_name=_("fertilizer german label"),
        unique=True,
        db_index=True,
    )

    def __str__(self):
        return self.id

    class Meta:
        db_table = "fertilizer"
        verbose_name = _("fertilizer")
        verbose_name_plural = _("fertilizers")
        ordering = ["id"]


class SpreadingRequirement(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=255,
        verbose_name=_("identifier"),
        unique=True,
        db_index=True,
    )

    label = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        verbose_name=_("spreading requirement french label"),
        unique=True,
        db_index=True,
    )

    label_de = models.CharField(
        null=True,
        blank=False,
        max_length=255,
        verbose_name=_("spreading requirement german label"),
        unique=True,
        db_index=True,
    )

    def __str__(self):
        return self.id

    class Meta:
        db_table = "spreading_requirement"
        verbose_name = _("spreading requirement")
        verbose_name_plural = _("spreading requirements")
        ordering = ["id"]


class SpreadingPeriod(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    crop_designation = models.ForeignKey(
        to="nitrogen_limitation.CropDesignation",
        related_name="spreading_period_crop_designation",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("crop designation"),
    )

    fertilizer = models.ForeignKey(
        to="nitrogen_limitation.Fertilizer",
        related_name="spreading_period_fertilizer",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("fertilizer"),
    )

    nvz_specific = models.BooleanField(
        blank=False,
        null=False,
        verbose_name=_("specific to vulnerable zones"),
        help_text=_(
            "Spreading period only applicable to plots inside nitrate vulnerable zones"
        ),
    )

    display_order = models.IntegerField(
        blank=False,
        null=False,
        verbose_name=_("Display order to the client"),
        help_text=_("The order of display for the period to the client, from 1 to n"),
    )

    description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("authorization description in French"),
    )

    description_de = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("authorization description in German"),
    )

    start_at = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        verbose_name=_("start date in French"),
        help_text=_("Start of the spreading period in French"),
    )

    end_at = models.CharField(
        null=False,
        blank=False,
        max_length=255,
        verbose_name=_("end date in French"),
        help_text=_("End of the spreading period in French"),
    )

    start_at_de = models.CharField(
        null=True,
        blank=False,
        max_length=255,
        verbose_name=_("start date in German"),
        help_text=_("Start of the spreading period in German"),
    )

    end_at_de = models.CharField(
        null=True,
        blank=False,
        max_length=255,
        verbose_name=_("end date in German"),
        help_text=_("End of the spreading period in German"),
    )

    def __str__(self):
        nvz_specific_str = _("inside vulnerable zone") if self.nvz_specific else ""
        return str(f"{self.crop_designation} {nvz_specific_str}").strip()

    class Meta:
        db_table = "spreading_period"
        verbose_name = _("spreading period")
        verbose_name_plural = _("spreading periods")
        ordering = ["id"]
        unique_together = (
            "crop_designation",
            "fertilizer",
            "nvz_specific",
            "display_order",
        )


class SpreadingCondition(models.Model):

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    spreading_requirement = models.ForeignKey(
        to="nitrogen_limitation.SpreadingRequirement",
        related_name="spreading_condition_requirement",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("spreading requirement"),
    )

    fertilizer = models.ForeignKey(
        to="nitrogen_limitation.Fertilizer",
        related_name="spreading_condition_fertilizer",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("fertilizer"),
    )

    content = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("authorization in French"),
    )

    content_de = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("authorization in German"),
    )

    def __str__(self):
        return str(f"{self.spreading_requirement} {self.fertilizer}").strip()

    class Meta:
        db_table = "spreading_condition"
        verbose_name = _("spreading condition")
        verbose_name_plural = _("spreading conditions")
        ordering = ["id"]
        unique_together = ("spreading_requirement", "fertilizer")


class Warning(models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    title = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        verbose_name=_("warning title in French"),
        db_index=True,
    )

    title_de = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        verbose_name=_("warning title in German"),
        db_index=True,
    )

    content = models.TextField(
        null=False,
        blank=False,
        verbose_name=_("warning content in French"),
    )

    content_de = models.TextField(
        null=True,
        blank=False,
        verbose_name=_("warning content in German"),
    )

    url = models.URLField(
        null=True, blank=True, verbose_name=_("URL of the regulation in French")
    )

    url_de = models.URLField(
        null=True, blank=True, verbose_name=_("URL of the regulation in German")
    )

    def __str__(self):
        return self.id

    class Meta:
        db_table = "warning"
        verbose_name = _("warning")
        verbose_name_plural = _("warnings")
        ordering = ["id"]
