import glob
from pathlib import Path
import json
import os
import subprocess

from db.graphql_clients import FastplatformClient
from gql import gql

from django.test import TestCase


class GraphQLTestCase(TestCase):
    """Test the GraphQL endpoint for all the exposed nodes

    Watch out! The tests are designed to run with the values in the init files,
    so if the init files are modified, the tests need to be modified as well.
    """

    @classmethod
    def setUpClass(cls):
        """Fill in the database with our init files"""
        super().setUpClass()
        env = os.environ.copy()
        env["POSTGRES_DATABASE"] = f"test_{os.environ.get('POSTGRES_DATABASE')}"
        subprocess.call(["sh", "./scripts/clear_data.sh"], env=env)
        subprocess.call(["sh", "./scripts/init_data.sh"], env=env)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_graphql_queries(self):
        """Test all the queries from the tests/graphql folder and make sure we get the corresponding response"""

        filepaths = glob.glob(str(Path(__file__).parent / "graphql/*.graphql"))
        filepaths.sort()

        # RTemove setups
        filepaths = [
            filepath for filepath in filepaths if not ".setup.graphql" in filepath
        ]

        for filepath in filepaths:
            filepath = Path(filepath)

            with self.subTest(path=filepath.name):

                # Read the query and its corresponding expected json
                query = filepath.read_text()
                if filepath.with_suffix(".variables.json").exists():
                    variables = json.loads(
                        filepath.with_suffix(".variables.json").read_text()
                    )
                else:
                    variables = {}

                if filepath.with_suffix(".headers.json").exists():
                    headers = json.loads(
                        filepath.with_suffix(".headers.json").read_text()
                    )
                else:
                    headers = {}

                if filepath.with_suffix(".setup.graphql").exists():
                    setup = filepath.with_suffix(".setup.graphql").read_text()
                    response = FastplatformClient().execute(gql(setup), {})

                expected_json = json.loads(filepath.with_suffix(".json").read_text())

                # Submit and assert results are identical
                response = self.client.post(
                    path="/graphql",
                    data=json.dumps({"query": query, "variables": variables}),
                    content_type="application/json",
                    **headers,
                )

                response_json = json.loads(response.content)

                # Keep for easier debug:
                # print()
                # print()
                # print("-- ", filepath.name, "---")
                # print("RESPONSE", json.dumps(response_json, ensure_ascii=False))
                # print("EXPECTED", json.dumps(expected_json, ensure_ascii=False))
                # print("------------------------")

                self.assertDictEqual(response_json, expected_json)
