import asyncio
import copy
import json
from unittest.case import TestCase

from nitrogen_limitation.schema.query import NitrogenLimitationQuery

from nitrogen_limitation.errors import GraphQLServiceError, GraphQLPlotNotFoundError
from . import mock_data as md
from nitrogen_limitation.schema.query import PARAMS, FERTILIZER_PARAM


class MockInfo:
    class MockContext:
        META = {
            "HTTP_X_HASURA_USER_ID": md.USER_ID,
            "HTTP_X_HASURA_ROLE": md.ROLE,
            "HTTP_X_HASURA_LANGUAGE": md.LANGUAGE,
        }

    context = MockContext()


class MockInput:
    params = []


class MockFastplatformClient:
    additional_headers: dict
    return_value: any

    def __init__(self, additional_headers={}) -> None:
        self.additional_headers = additional_headers

    def execute(self, query, variables):
        return self.return_value


class MockCropDesignation:
    id: str
    name: str
    max_kg_organic_n_per_ha_per_year: float
    organic_n_comment: str
    max_kg_organic_plus_mineral_n_per_ha_per_year: float
    organic_plus_mineral_n_comment: str


class NitrogenLimitationTest(TestCase):
    loop = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.loop = asyncio.get_event_loop()
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.loop.close()
        return super().tearDownClass()

    def setUp(self) -> None:
        return super().setUp()

    def run_async(self, method):
        data = self.loop.run_until_complete(method)
        return data

    def test_get_x_hasura_user_id_and_role(self):
        info = MockInfo()

        # Assert does not raise when user_id and role in headers
        user_id, role = NitrogenLimitationQuery.get_x_hasura_user_id_and_role(info)

        self.assertEqual(user_id, md.USER_ID)
        self.assertEqual(role, md.ROLE)

        # Assert raises if one is missing
        incomplete_info = copy.deepcopy(info)
        del incomplete_info.context.META["HTTP_X_HASURA_USER_ID"]
        with self.assertRaises(GraphQLServiceError):
            NitrogenLimitationQuery.get_x_hasura_user_id_and_role(incomplete_info)

        incomplete_info = copy.deepcopy(info)
        del incomplete_info.context.META["HTTP_X_HASURA_ROLE"]
        with self.assertRaises(GraphQLServiceError):
            NitrogenLimitationQuery.get_x_hasura_user_id_and_role(incomplete_info)

    def test_build_params_node(self):
        mock_input = MockInput()
        mock_input.params = json.dumps({})

        params_node, params = NitrogenLimitationQuery.build_params_node(
            mock_input, "fr"
        )

        self.assertIsInstance(params_node, list)
        self.assertEqual(len(params_node), len(PARAMS))
        for param in params_node:
            self.assertIsNone(param.value)

        self.assertIsInstance(params, dict)
        self.assertTrue(FERTILIZER_PARAM in params)
        self.assertIsNone(params[FERTILIZER_PARAM])

    def test_get_params_with_complete_params(self):
        input = MockInput()
        value_expected = "FERTILISANT_MINERAL"
        input.params = {"FERTILIZER_PARAM": value_expected}

        # convert to string to simulate genericScalar
        input.params = json.dumps(input.params)

        params_node, params = NitrogenLimitationQuery.build_params_node(input, "fr")

        self.assertIsInstance(params_node, list)
        self.assertEqual(len(params_node), len(PARAMS))
        for param in params_node:
            self.assertIsNotNone(param.value)
            self.assertEqual(param.value, value_expected)

        self.assertIsInstance(params, dict)
        self.assertTrue(FERTILIZER_PARAM in params)
        self.assertIsNotNone(params[FERTILIZER_PARAM])
        self.assertEqual(params[FERTILIZER_PARAM], value_expected)

    def test_fastplatform_client_has_user_headers_injected(self):
        mock_fp_client = NitrogenLimitationQuery.get_fastplatform_client(
            md.USER_ID, md.ROLE, MockFastplatformClient
        )

        self.assertTrue("X-Hasura-User-Id" in mock_fp_client.additional_headers)
        self.assertEqual(
            mock_fp_client.additional_headers["X-Hasura-User-Id"], md.USER_ID
        )

        self.assertTrue("X-Hasura-Role" in mock_fp_client.additional_headers)
        self.assertEqual(mock_fp_client.additional_headers["X-Hasura-Role"], md.ROLE)

    def test_get_plot_plant_species_id_raises(self):
        mock_fastplatform_client = MockFastplatformClient()
        values_raising_service_error = [
            None,
            [],
            {"no_plot_key": "value"},
            {"plot": {}},
        ]
        for invalid_value in values_raising_service_error:
            with self.subTest(service_error_value=invalid_value):
                mock_fastplatform_client.return_value = invalid_value
                with self.assertRaises(GraphQLServiceError):
                    NitrogenLimitationQuery.get_base_params(
                        md.PLOT_ID, mock_fastplatform_client
                    )

        values_raising_plot_not_found_error = [
            {"plot": []},
        ]

        for invalid_value in values_raising_plot_not_found_error:
            invalid_value[
                "computed_constraints_management_restriction_or_regulation_zone"
            ] = None
            with self.subTest(plot_not_found_value=invalid_value):
                with self.assertRaises(GraphQLPlotNotFoundError):
                    mock_fastplatform_client.return_value = invalid_value
                    NitrogenLimitationQuery.get_base_params(
                        md.PLOT_ID, mock_fastplatform_client
                    )

    def test_plot_plant_species_missing_base_params(self):
        mock_fastplatform_client = MockFastplatformClient()
        values_missing_base_params = [
            {
                "computed_constraints_management_restriction_or_regulation_zone": None,
                "plot": [
                    {"plot_plant_variety": {}},
                ],
            },
            {
                "computed_constraints_management_restriction_or_regulation_zone": None,
                "plot": [
                    {"plot_plant_variety": {"plant_variety": {}}},
                ],
            },
            {
                "computed_constraints_management_restriction_or_regulation_zone": None,
                "plot": [
                    {"plot_plant_variety": {"plant_variety": {"plant_species": {}}}},
                ],
            },
        ]

        for value in values_missing_base_params:
            with self.subTest(value=value):
                mock_fastplatform_client.return_value = value
                base_params = NitrogenLimitationQuery.get_base_params(
                    md.PLOT_ID, mock_fastplatform_client
                )

                self.assertIsInstance(base_params, dict)
                self.assertTrue("plant_species_id" not in base_params)

    def test_computed_constraints_management_restriction_or_regulation_zone(self):
        mock_fastplatform_client = MockFastplatformClient()
        return_value = {
            "computed_constraints_management_restriction_or_regulation_zone": None,
            "plot": [
                {
                    "plot_plant_variety": {
                        "id": 13,
                        "plant_variety": {
                            "id": md.PLANT_SPECIES_ID,
                            "name": md.PLANT_SPECIES_ID,
                            "plant_species": {
                                "id": md.PLANT_SPECIES_ID,
                                "name": md.PLANT_SPECIES_ID,
                            },
                        },
                    }
                }
            ],
        }

        existing = [
            {
                "name": "management_restriction_or_regulation_zone",
                "description": {
                    "data": {
                        "management_restriction_or_regulation_zone_id": 1,
                        "management_restriction_or_regulation_zone_name": "name",
                        "plot_pct_in_management_restriction_or_regulation_zone": 0.333,
                        "plot_area_in_management_restriction_or_regulation_zone": 333,
                    }
                },
                "plot_id": 1,
            }
        ]

        constraints = [None, existing]

        for constraint in constraints:
            with self.subTest(constraint=constraint):
                return_value[
                    "computed_constraints_management_restriction_or_regulation_zone"
                ] = constraint
                mock_fastplatform_client.return_value = return_value

                base_params = NitrogenLimitationQuery.get_base_params(
                    md.PLOT_ID, mock_fastplatform_client
                )

                self.assertIsInstance(base_params, dict)
                self.assertTrue("plant_species_id" in base_params)

                self.assertEqual(
                    base_params["is_in_management_restriction_or_regulation_zone"],
                    not constraint is None,
                )

    def test_get_plot_plant_species_id_nominal_case_ok(self):
        mock_fastplatform_client = MockFastplatformClient()

        mock_fastplatform_client.return_value = {
            "computed_constraints_management_restriction_or_regulation_zone": None,
            "plot": [
                {
                    "plot_plant_variety": {
                        "id": 13,
                        "plant_variety": {
                            "id": md.PLANT_SPECIES_ID,
                            "name": md.PLANT_SPECIES_ID,
                            "plant_species": {
                                "id": md.PLANT_SPECIES_ID,
                                "name": md.PLANT_SPECIES_ID,
                            },
                        },
                    }
                }
            ],
        }

        base_params = NitrogenLimitationQuery.get_base_params(
            md.PLOT_ID, mock_fastplatform_client
        )

        self.assertEqual(base_params["plant_species_id"], md.PLANT_SPECIES_ID)
