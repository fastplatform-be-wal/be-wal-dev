from django.contrib import admin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from import_export.admin import ImportExportMixin

from nitrogen_limitation.models import (
    CropDesignation,
    PlantSpeciesToCropDesignation,
    Fertilizer,
    SpreadingRequirement,
    SpreadingPeriod,
    SpreadingCondition,
    Warning,
)

from nitrogen_limitation.resources import (
    CropDesignationResource,
    PlantSpeciesToCropDesignationResource,
    FertilizerResource,
    SpreadingRequirementResource,
    SpreadingPeriodResource,
    SpreadingConditionResource,
    WarningResource,
    UserResource,
)


class UserAdmin(ImportExportMixin, admin.ModelAdmin):
    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_active",
        "date_joined",
        "is_staff",
    )

    resource_class = UserResource


admin.site.unregister(User)
admin.site.register(User, UserAdmin)


@admin.register(CropDesignation)
class CropDesignationAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = CropDesignationResource
    list_display = [
        "id",
        "name",
        "max_kg_organic_n_per_ha_per_year",
        "organic_n_comment",
        "organic_n_comment_de",
        "max_kg_organic_plus_mineral_n_per_ha_per_year",
        "organic_plus_mineral_n_comment",
        "organic_plus_mineral_n_comment_de",
    ]
    list_display_links = ["id", "name"]


@admin.register(PlantSpeciesToCropDesignation)
class PlantSpeciesToCropDesignationAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = PlantSpeciesToCropDesignationResource
    list_display = [
        "plant_species_id",
        "crop_designation_name",
    ]
    list_display_links = ["plant_species_id"]

    def crop_designation_name(self, obj):
        return obj.crop_designation.name

    crop_designation_name.short_description = _("crop designation name")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("crop_designation")
        return queryset


@admin.register(Fertilizer)
class FertilizerAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = FertilizerResource
    list_display = [
        "id",
        "label",
        "label_de",
    ]
    list_display_links = ["id"]


@admin.register(SpreadingRequirement)
class SpreadingRequirementAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = SpreadingRequirementResource
    list_display = [
        "id",
        "label",
        "label_de",
    ]
    list_display_links = ["id"]


@admin.register(SpreadingPeriod)
class SpreadingPeriodAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = SpreadingPeriodResource
    list_display = [
        "id",
        "crop_designation",
        "fertilizer_label",
        "nvz_specific",
        "display_order",
        "description",
        "description_de",
        "start_at",
        "end_at",
        "start_at_de",
        "end_at_de",
    ]
    list_display_links = ["id"]

    def fertilizer_label(self, obj):
        return obj.fertilizer.label

    fertilizer_label.short_description = _("fertilizer")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("fertilizer")
        return queryset


@admin.register(SpreadingCondition)
class SpreadingConditionAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = SpreadingConditionResource
    list_display = [
        "id",
        "spreading_requirement_label",
        "fertilizer_label",
        "content",
        "content_de",
    ]
    list_display_links = ["id"]

    def fertilizer_label(self, obj):
        return obj.fertilizer.label

    fertilizer_label.short_description = _("fertilizer label")

    def spreading_requirement_label(self, obj):
        return obj.spreading_requirement.label

    spreading_requirement_label.short_description = _("spreading requirement label")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related("fertilizer").select_related(
            "spreading_requirement"
        )
        return queryset


@admin.register(Warning)
class WarningAdmin(ImportExportMixin, admin.ModelAdmin):
    resource_class = WarningResource
    list_display = [
        "id",
        "title",
        "title_de",
        "content",
        "content_de",
        "url",
        "url_de",
    ]
    list_display_links = ["id", "title"]
