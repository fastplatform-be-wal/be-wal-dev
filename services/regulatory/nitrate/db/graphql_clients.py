import os
from gql import Client
from gql.transport.requests import RequestsHTTPTransport
from gql.client import SyncClientSession
from graphql import print_ast, ExecutionResult
import requests
from gql.transport.exceptions import (
    TransportClosed,
    TransportProtocolError,
    TransportServerError,
)


class RequestsHTTPTransportWithExtraHeaders(RequestsHTTPTransport):
    def execute(  # type: ignore
        self,
        document,
        variable_values=None,
        operation_name=None,
        timeout=None,
        extra_headers=None,  # this is new
    ):
        """Override of the original RequestsHTTPTransport.execute function
        to allow passing extra headers at execution time
        """

        if not self.session:
            raise TransportClosed("Transport is not connected")

        query_str = print_ast(document)
        payload = {"query": query_str}
        if variable_values:
            payload["variables"] = variable_values
        if operation_name:
            payload["operationName"] = operation_name

        data_key = "json" if self.use_json else "data"
        post_args = {
            "headers": self.headers,
            "auth": self.auth,
            "cookies": self.cookies,
            "timeout": timeout or self.default_timeout,
            "verify": self.verify,
            data_key: payload,
        }

        # Pass kwargs to requests post method
        post_args.update(self.kwargs)

        # This is new as well
        if extra_headers:
            post_args["headers"].update(extra_headers)

        # Using the created session to perform requests
        response = self.session.request(
            self.method, self.url, **post_args  # type: ignore
        )
        try:
            result = response.json()
        except Exception:
            # We raise a TransportServerError if the status code is 400 or higher
            # We raise a TransportProtocolError in the other cases

            try:
                # Raise a requests.HTTPerror if response status is 400 or higher
                response.raise_for_status()

            except requests.HTTPError as e:
                raise TransportServerError(str(e))

            raise TransportProtocolError("Server did not return a GraphQL result")

        if "errors" not in result and "data" not in result:
            raise TransportProtocolError("Server did not return a GraphQL result")

        return ExecutionResult(errors=result.get("errors"), data=result.get("data"))


class GraphQLClient:

    name: str
    url: str
    headers: dict
    client: Client
    session: SyncClientSession

    def __init__(self, name: str, url: str, headers: dict):
        self.name = name
        self.headers = headers
        self.url = url

        self.transport = RequestsHTTPTransportWithExtraHeaders(
            url=self.url,
            headers=self.headers,
        )
        self.client = None
        self.session = None

    def connect(self):
        self.client = Client(transport=self.transport, fetch_schema_from_transport=True)
        self.client.transport.connect()
        self.session = SyncClientSession(client=self.client)

    def execute(self, *args, **kwargs):
        return self.session.execute(*args, **kwargs)

    def close(self):
        self.client.transport.close()


class FastplatformClient:
    graphql_client: GraphQLClient

    def __init__(self, additional_headers={}):
        url = os.environ.get("API_GATEWAY_FASTPLATFORM_URL")
        service_key = os.environ.get("API_GATEWAY_SERVICE_KEY")

        headers = {
            "Authorization": f"Service {service_key}",
            "Content-type": "application/json",
        }
        headers.update(additional_headers)

        self.graphql_client = GraphQLClient(
            name="fastplatform", url=url, headers=headers
        )

    def execute(self, *args, **kwargs):
        self.graphql_client.connect()

        try:
            return self.graphql_client.execute(*args, **kwargs)
        except Exception as ex:
            raise ex
        finally:
            self.graphql_client.close()
