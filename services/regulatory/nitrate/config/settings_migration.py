import os

from .settings import *

DATABASES["default"]["HOST"] = os.environ.get("POSTGRES_SUPER_HOST", DATABASES["default"]["HOST"])
DATABASES["default"]["PORT"] = os.environ.get("POSTGRES_SUPER_PORT", DATABASES["default"]["PORT"])
DATABASES["default"]["USER"] = os.environ.get("POSTGRES_SUPER_USER", DATABASES["default"]["USER"])
DATABASES["default"]["PASSWORD"] = os.environ.get("POSTGRES_SUPER_PASSWORD", DATABASES["default"]["PASSWORD"])
