import os
from pathlib import Path

BASE_DIR = Path(__file__).parent.parent

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")
DEBUG = os.environ.get("DEBUG", "TRUE").upper() == "TRUE"
DOMAIN_NAME = os.environ.get("DOMAIN_NAME")
ALLOWED_HOSTS = [DOMAIN_NAME] + os.environ.get("DJANGO_ALLOWED_HOSTS", "").split(",")
if DEBUG:
    ALLOWED_HOSTS = [
        "localhost",
        "localhost.fastplatform.eu",
        "host.docker.internal",
    ] + ALLOWED_HOSTS
    CORS_ALLOW_ALL_ORIGINS = True
    INTERNAL_IPS = ["127.0.0.1", "192.168.1.33"]

SILENCED_SYSTEM_CHECKS = ["admin.E410"]


# Application definition
# ----------------------
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.staticfiles",
    "graphene_django",
    "import_export",
    "constance.backends.database",
    "config.apps.ParametersConfig",
    "nitrogen_limitation.apps.NitrogenLimitationConfig",
]


if DEBUG:
    INSTALLED_APPS += [
        "sslserver",
        "django_extensions",
        "corsheaders",
        "debug_toolbar",
    ]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

if DEBUG:
    MIDDLEWARE += [
        "debug_toolbar.middleware.DebugToolbarMiddleware",
        "corsheaders.middleware.CorsMiddleware",
    ]

ROOT_URLCONF = "config.urls"
WSGI_APPLICATION = "config.wsgi.application"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
            ]
        },
    },
]

# Database
# --------
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DATABASE"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": os.environ.get("POSTGRES_HOST"),
        "PORT": os.environ.get("POSTGRES_PORT"),
        "CONN_MAX_AGE": int(os.environ.get("POSTGRES_CONN_MAX_AGE", 0)),
        "DISABLE_SERVER_SIDE_CURSORS": os.environ.get(
            "DISABLE_SERVER_SIDE_CURSORS", "TRUE"
        ).upper()
        == "TRUE",
    }
}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"


# Internationalization
# --------------------
LANGUAGE_CODE = os.environ.get("LANGUAGE_CODE", "fr")

LANGUAGES = (
    ("bg", "Bulgarian / Български"),
    ("de", "German / Deutsch"),
    ("el", "Greek / Ελληνική"),
    ("en", "English"),
    ("es", "Spanish / Castellano"),
    ("et", "Estonian / Eesti"),
    ("fr", "French / Français"),
    ("it", "Italian / Italiano"),
    ("ro", "Romanian / Română"),
    ("sk", "Slovak / Slovenčina"),
)
TIME_ZONE = os.environ.get("TIME_ZONE", "UTC")
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = (BASE_DIR / "locale",)

# GraphQL
# -------
GRAPHENE = {"SCHEMA": "config.schema.schema"}

# Messages
# --------
MESSAGE_STORAGE = "django.contrib.messages.storage.session.SessionStorage"

# Constance
# ---------
from nitrogen_limitation.config import (
    NITROGEN_LIMITATION_CONFIG,
    NITROGEN_LIMITATION_CONFIG_FIELDSETS,
)

CONSTANCE_CONFIG = NITROGEN_LIMITATION_CONFIG
CONSTANCE_CONFIG_FIELDSETS = NITROGEN_LIMITATION_CONFIG_FIELDSETS
CONSTANCE_BACKEND = "constance.backends.database.DatabaseBackend"
CONSTANCE_IGNORE_ADMIN_VERSION_CHECK = True

# Security
# --------
SECURE_HSTS_SECONDS = int(os.environ.get("DJANGO_SECURE_HSTS_SECONDS", 0))
if SECURE_HSTS_SECONDS > 0:
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True
    SECURE_HSTS_PRELOAD = True

SECURE_REFERRER_POLICY = "same-origin"
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
USE_X_FORWARDED_HOST = True
SECURE_REFERRER_POLICY = "same-origin"
CSRF_COOKIE_SECURE = (
    os.environ.get("DJANGO_CSRF_COOKIE_SECURE", "TRUE").upper() == "TRUE"
)

# Static files
# ------------
# Static files are served by whitenoise
STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR / "static"
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# Logging
# -------

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
        },
    },
    "root": {
        "handlers": ["console"],
        "level": os.environ.get("DJANGO_LOG_LEVEL", "INFO").upper(),
    },
}
