import graphene
from django.conf import settings

from nitrogen_limitation.schema.query import NitrogenLimitationQuery

query_classes = [NitrogenLimitationQuery, graphene.ObjectType]


class Query(*query_classes):
    nitrogen_limitation__healthz = graphene.String(
        default_value="OK", description="Liveliness probe"
    )


schema = graphene.Schema(query=Query, auto_camelcase=False)
