# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

SERVICE_HOST=0.0.0.0
SERVICE_PORT=7778

include secrets.env
export $(shell sed 's/=.*//' secrets.env)
include service.env
export $(shell sed 's/=.*//' service.env)

.PHONY: start
start: ## Start the service locally on port 7777
	uvicorn \
	app.main:app \
	--host=$(SERVICE_HOST) \
	--port=$(SERVICE_PORT) \
	--reload \
	--log-config logging-dev.json

TEST_USER=
.PHONY: test-insert-demo-holding
test-insert-demo-holding:  # Insert the demo holding for user TEST_USER
	curl -X POST \
		-d '$(shell cat app/tests/insert_demo_holding.json)' \
		-H "Authorization: Service $(HASURA_SERVICE_SHARED_SECRET_KEY)" \
		-H "X-Hasura-User-Id: $(TEST_USER)" \
		-H "Content-Type: application/json" \
		http://localhost:7777/graphql

.PHONY: test-sync-holdings
test-sync-holdings:  # Sync holdings from the IACS database for user TEST_USER
	curl -X POST \
		-d '$(shell cat app/tests/sync_holdings.json)' \
		-H "Authorization: Service $(HASURA_SERVICE_SHARED_SECRET_KEY)" \
		-H "X-Hasura-User-Id: $(TEST_USER)" \
		-H "Content-Type: application/json" \
		http://localhost:7777/graphql

HOLDING_ID=
.PHONY: test-sync-holding
test-sync-holding:  # Sync the plots of holding HOLDING_ID and user TEST_USER from the IACS database
	curl -X POST \
		-d '$(shell cat app/tests/sync_holding.json | sed "s/HOLDING_ID/${HOLDING_ID}/g")' \
		-H "Authorization: Service $(HASURA_SERVICE_SHARED_SECRET_KEY)" \
		-H "X-Hasura-User-Id: $(TEST_USER)" \
		-H "X-Hasura-Role: farmer" \
		-H "Content-Type: application/json" \
		http://localhost:7777/graphql

.PHONY: start-tracing
start-tracing: ## Start the Jaeger tracing service
	docker run --rm --name tracing-fastplatform \
		-e COLLECTOR_ZIPKIN_HOST_PORT=:9411 \
		-p 9411:9411 \
		-p 16686:16686 \
		-d docker.io/jaegertracing/all-in-one

.PHONY: stop-tracing
stop-tracing: ## Stop the Jaeger tracing service
	-docker stop tracing-fastplatform

.PHONY: bazel-run
bazel-run: ## Run the service locally with bazel
	bazel run .

.PHONY: bazel-test
test:  ## Run all test suite
	coverage run --rcfile=.coveragerc \
				 -m unittest tests/*/test_*.py

.PHONY: report
report:  ## Build and open the test report
	coverage html
	open ./.coverage_html/index.html

.PHONY: unit-test
unit-test: ## Run only unit tests
	coverage run --rcfile=.coveragerc \
				 -m unittest tests/unit_testing/test_iacs.py

.PHONY: query-test
query-test: ## Run only queries tests
	coverage run --rcfile=.coveragerc \
				 -m unittest tests/query_testing/test_queries.py 

USER_IDS=
.PHONY: test-users
test-users:
	python -m tests.create_test_users ${USER_IDS}

# Pip compile (Python dependencies)
# =====================

.PHONY: pip-compile
pip-compile: ## Lock Python dependencies
	pip-compile --allow-unsafe --generate-hashes requirements.in