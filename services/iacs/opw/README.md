# `be-wal/iacs/opw`: IACS connector for OPW

The [be-wal/iacs/opw](./) service is a connector to access IACS/GSAA data on the OPW IACS APIs and serve these capabilities to the FaST farmer application as GraphQL queries and mutations.

- [`be-wal/iacs/opw`: IACS connector for OPW](#be-waliacsopw-iacs-connector-for-opw)
  - [GraphQL server](#graphql-server)
      - [Exceptions](#exceptions)
  - [OPW APIs](#opw-apis)
      - [API authentication](#api-authentication)
          - [Determining the farms of a user](#determining-the-farms-of-a-user)
          - [Determining the plots of a farm](#determining-the-plots-of-a-farm)
  - [Environment variables](#environment-variables)
  - [Development](#development)
        - [Set up the database and the API:](#set-up-the-database-and-the-api)
        - [Run the service:](#run-the-service)
    - [Management commands](#management-commands)
  - [Running the tests](#running-the-tests)

## GraphQL server

The service exposes a GraphQL server on the `/graphql` endpoint. The following (high-level) GraphQL schema is exposed:

```graphql
query {
    # OPW IACS metadata
    iacs_display_name
    iacs_short_name
    iacs_website_url
}

query {
    # Health check
    nitrogen_limitation__healthz
}

mutation {
    # Request the holdings that are accessible to the user (in the OPW IACS) and create/update the corresponding Holdings and UserRelatedParties in the FaST database
    sync_holdings {
        holding_ids
        messages
    }

    # Query the lines of the CAP submission of a given holding, and convert those to HoldingCampaign, Plots and Site objects in the FaST database.
    sync_holding(holding_id, campaign_id, iacs_year) {
        plot_ids
        messages
    }

    # Create a demo holding for the user (where the user is the only UserRelatedParty), based on a fixed description of site/geometry available in the `demo/holding.json` file of this repository.
    insert_demo_holding(campaign_id) {
        holding_id
    }

    # Copy the farm holding definition (Site, Plots, Plant Varieties) from one campaign to another.
    duplicate_holding_campaign(holding_id, campaign_from_id, campaign_to_id) {
        holding_campaign_id
    }
}

# The service does not expose any subscription.
```

The schema is self documenting and we encourage the reader to introspect it to learn more about each node and its associated parameters and fields. This can done using the [GraphiQL](https://github.com/graphql/graphiql) or [Altair](https://altair.sirmuel.design/) tools, for example.

<div align="center">
    <img src="docs/img/graphql-schema.png" width="420">
</div>

Several example queries are provided for each node, as part of the test suite: [tests/query_testing/queries/](tests/query_testing/queries/).

#### Exceptions

The service raises several GraphQL error types:
- `IACS_USER_UNAUTHENTICATED`: The user is not authenticated to query the requested data (e.g. token expired).
- `IACS_USER_FORBIDDEN`: The user is not allowed, byt he Paying Agency, to query the requested data on their APIs
- `IACS_OBJECT_NOT_FOUND`: The user or the requested farm/holding does not exist in the Paying Agency database.
- `IACS_SERVICE_ERROR`: An unrecoverable error occurred on the FaST service side (e.g. query timeout)
- `IACS_REMOTE_ERROR`: An error occurred on the Paying Agency API side (e.g. FaST API unavailable)

## OPW APIs

The following concepts are mapped between the OPW semantics and FaST semantics:
- FaST Holding = OPW Producteur
- FaST Plot = OPW Parcelle
- FaST User identifier = Wallonian Numéro de Registre National (NRN)

The OPW exposes 2 REST-like endpoints:
- `/producteurs/numero-national/{NUMERO_NATIONAL}`: returns the metadata of the farms/producteurs that the user with the provided `NUMERO_NATIONAL` is entitled to access
- `/producteurs/{PRODUCTEUR_ID}/numero-national/{NUMERO_NATIONAL}/parcelles`: returns the geometries of the plots/parcelles of the farm/producteur `PRODUCTEUR_ID` that the user with `NUMERO_NATIONAL` is entitled to access

#### API authentication

The FaST backend authenticates itself against the OPW APIs using a JWT signed with a shared secret key. The token expires after a few minutes and is automatically re-generated upon expiry.

###### Determining the farms of a user

The *producteur* objects returned by the OPW API are mapped one-to-one with the corresponding FaST Holding objects, as follows:

```plantuml
package "Context" as context <<Rectangle>> {
    class CurrentUser {
        username
    }
}

package "OPW IACS APIs" as apis <<Rectangle>> {
    class get_producteurs {
        id
        nom
        role
    }
}

cloud <<APIs>>\nOPW as opw {
    skinparam defaultTextAlignment center
}

package "FaST Data Model" as fast_data_model <<Rectangle>> {
    class Holding {
        id
        name
    }

    class UserRelatedParty {
        user : User
        role
    }

    Holding "1" -up-> "0..*" UserRelatedParty
}

opw <-[hidden]- apis
context -[hidden] apis

CurrentUser::username -left[#blue]-> UserRelatedParty::user
get_producteurs::id -left[#blue]-> Holding::id
get_producteurs::nom -left[#blue]-> Holding::name
get_producteurs::role -left[#blue]-> UserRelatedParty::role
apis -r[#blue]-> opw

```

###### Determining the plots of a farm

The *parcelle* objects returned by the OPW API are mapped one-to-one with the corresponding FaST Plot objects, as follows:

```plantuml
package "Context" as context <<Rectangle>> {
    class Query {
        holding_id
        campaign_id
    }
}

package "OPW IACS APIs"  as apis <<Rectangle>> {
    class get_parcelles {
        id
        nom
        geometrie
        cultureCode
    }
}

cloud <<APIs>>\nOPW as opw {
    skinparam defaultTextAlignment center
}

package "FaST Data Model" as c <<Rectangle>> {
    class Campaign {
        id
    }

    class HoldingCampaign {
        campaign_id
        holding_id
    }

    class Site {
        authority_id
        name
    }

    class Plot {
        authority_id
        name
        geometry
    }

    class PlotPlantVariety {
        plant_variety : PlantVariety
    }
    
    Campaign "1" -up-> "0..*" HoldingCampaign
    HoldingCampaign "1" -up-> "0..*" Site
    Site "1" -up-> "0..*" Plot
    Plot "1" -up-> "0..*" PlotPlantVariety
}


Query::holding_id -right[#blue]-> HoldingCampaign::holding_id
Query::holding_id -right[#blue]-> Site::authority_id
Query::campaign_id -right[#blue]-> HoldingCampaign::campaign_id
Query::campaign_id -right[#blue]-> Campaign::id

get_parcelles::id -left[#blue]-> Plot::authority_id
get_parcelles::nom -left[#blue]-> Plot::name
get_parcelles::geometrie -left[#blue]-> Plot::geometry
get_parcelles::cultureCode -left[#blue]-> PlotPlantVariety::plant_variety
apis -down[#blue]-> opw

```

## Environment variables
The environment variables used by the service are listed in the [settings file](app/settings.py).

FaST API gateway:
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the FaST `fastplatform` API gateway. 
- `API_GATEWAY_SERVICE_KEY`: secret key of the FaST API gateways service account.
- `API_GATEWAY_TIMEOUT`: The timeout value in seconds used to access fastplatform gateway. Defaults to `120` (= 2 minutes), because some farms can have thousands of plots to insert.

OPW APIs:
- `OPW_API_ROOT`: The API endpoint root URL. Defaults to `"https://agriculture.valid.wallonie.be/opw/gateway/fast/api/v1"`.
- `OPW_API_GET_PARCELLES_URL`: OPW API endpoint URL to query the plots (parcelles) of a holding (producteur). Defaults to `"{OPW_API_ROOT}/producteurs/{PRODUCTEUR_ID}/numero-national/{NUMERO_NATIONAL}/parcelles"`.
- `OPW_API_GET_PRODUCTEURS_URL`: OPW API endpoint URL to query the holdings (producteurs) a specific user has access to. Defaults to `"{OPW_API_ROOT}/producteurs/numero-national/{NUMERO_NATIONAL}"`.
- `OPW_TIMEOUT`: The timeout value in seconds for any query sent to the OPW API. Defaults to `60` (= 1 minute).
- `OPW_KID`: the key identifier used to generate the access token of the service.
- `OPW_SECRET_KEY`: secret key used to generate the access token of the service.
- `OPW_JWT_SIGNING_ALGORITHM`: The signing algorithm used for the JWT service token. Defaults to `"HS256"`.
- `OPW_TOKEN_VALIDITY_PERIOD`: The duration in seconds used to compute the `expiration_time` field of the token. Defaults to `3600` (= 1 hour).
- `OPW_GEOMETRY_EPSG`: The coordinate reference system (CRS) used to describe the plots geometries. Defaults to `"4326"` (= `EPSG:4326`).

Service:
- `CULTURE_CODES_IN_MESSAGE_TRUNCATE_AFTER`: The maximum number of different culture codes missing displayed to the user. Defaults to `3`.
- `NOT_ALL_PLOTS_INSERTED_MESSAGE`: A message warning the user that some plots have not been inserted. Defaults to `{PLOT_COUNT} parcelle(s) n'ont pas été ingérées`.
- `PLOTS_IN_MESSAGE_TRUNCATE_AFTER`: The maximum number of different plot ids not inserted displayed to the user. Defaults to `3`.
- `SOME_PLOTS_WITH_CULTURE_CODE_MISSING`: A message warning the user that some culture codes were not recognized, and were thus not included in the plot(s) inserted. Defaults to `{PLOT_COUNT} parcelle(s) ont un code culture non-reconnu`.
- `IACS_DISPLAY_NAME`: The full name of the OPW IACS. Defaults to `Organisme Payeur de Wallonie`.
- `IACS_SHORT_NAME`: The short name of the OPW IACS. Defaults to `OPW`.
- `IACS_WEBSITE_URL`: The url for the OPW IACS. Defaults to `https://agriculture.wallonie.be/organisme-payeur-de-wallonie-et-coordination`.

## Development

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

##### Set up the database and the API:

Start the [core/web/backend](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/web/backend) Postgres databases and make sure the [core/web/backend](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/web/backend) migrations are applied:
```bash
cd ../web/backend  # cd to wherever the core/web/backend service is on your disk
make start-postgres
make migrate
```

Load the Wallonian initial data:
```bash
cd ../web/backend  # cd to wherever the core/web/backend service is on your disk
make clear-region init-region REGION=be-wal
```

Start the [core/api_gateway/fastplatform](https://gitlab.com/fastplatform-be-wal/core/-/tree/master/services/api_gateway/fastplatform) service:
```bash
cd ../api_gateway/fastplatform  # cd to wherever the core/api_gateway/fastplatform service is on your disk
make start
```

##### Run the service:

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the development server with auto-reload enabled:
```bash
make start
```

The server is then ready to receive queries at http://localhost:7777/graphql.

### Management commands

The following management commands are available from the `Makefile`.

- Uvicorn targets:
    - `make start`: Start the development server with auto-reload enabled.

- Testing targets:
    - `make test`: Run the test suite
	
Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).

## Running the tests

If not already done, follow the steps to set up the database and the API, [above](#set-up-the-database-and-the-api).

The full test suite is composed of:
- [unit tests](tests/unit_testing)
- [query tests](tests/query_testing)

You can launch the full test suite or target specific tests:
```sh
make test  # Launch the full test suite
make unit-test  # Launch only unit tests (tests/unit_testing/test_iacs.py)
make query-test  # Launch only query tests (tests/query_testing/test_queries.py)
```
