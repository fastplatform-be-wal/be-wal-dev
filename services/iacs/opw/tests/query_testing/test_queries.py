"""
This tests require an instance of hasura up, running and accessible.

No other requirements expected in terms of db data prefilling,
they can run on empty db (only clear-region, no init-region).

BEWARE: running this tests will erase all data in certain tables.
"""
import os
from pathlib import Path
import glob
import json
from unittest import TestCase
import asyncio
from gql import gql
import time
from flatten_dict import flatten

from fastapi.testclient import TestClient

from app.main import app
from app.api.lib.opw import opw_client
from app.db.graphql_clients import fastplatform
from tests import MockApiClient


QUERIES_DIR = Path(__file__).parent / "queries"
CLEAR_DBS_MUTATION_URI = Path(__file__).parent / "mutation_clear_dbs.graphql"
SETUP_QUERY_URI = Path(__file__).parent / "setup.graphql"

# RUN_ONLY = "01-sync_holding"
RUN_ONLY = None


class QueriesTest(TestCase):
    clear_dbs_query = None
    setup_query = None
    loop = None

    # Counters
    ran = 0
    succeeded = 0

    @classmethod
    def setUpClass(cls) -> None:
        cls.clear_dbs_query = CLEAR_DBS_MUTATION_URI.read_text()
        cls.setup_query = SETUP_QUERY_URI.read_text()
        cls.loop = asyncio.get_event_loop()

        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        print()
        print(f"{QueriesTest.succeeded}/{QueriesTest.ran} query scenarios succeeded")

        return super().tearDownClass()

    def run_async(self, method):
        return self.loop.run_until_complete(method)

    def get_tested_queries(self):
        tested_queries = glob.glob(str(QUERIES_DIR / "*"))
        tested_queries.sort()
        for tested_query in tested_queries:
            # For easier debug, adding a file simply called 'skip' (no extension) skips the test
            if not Path(f"{tested_query}/skip").exists():
                dir = Path(tested_query).name
                name = dir[3:]  # ex: "01-sync_holding" -> "sync_holding"
                yield dir, name

    def load_api_return_value(self, query_dir, query_name):
        api = Path(str(QUERIES_DIR / query_dir / f"{query_name}.api.json"))
        if not api.exists():
            return None

        content = api.read_text()

        # return actual json
        return json.loads(content)

    def load_expected_struct(self, query_dir, query_name):
        expected_struct = Path(
            str(QUERIES_DIR / query_dir / f"{query_name}.expected-struct.json")
        )
        if not expected_struct.exists():
            return None

        content = expected_struct.read_text()

        # return actual json
        return json.loads(content)

    def load_expected(self, query_dir, query_name):
        expected = Path(str(QUERIES_DIR / query_dir / f"{query_name}.expected.json"))
        if not expected.exists():
            return None

        content = expected.read_text()

        # return actual json
        return json.loads(content)

    def load_headers(self, query_dir, query_name):
        headers = Path(str(QUERIES_DIR / query_dir / f"{query_name}.headers.json"))
        content = headers.read_text()

        # return actual json
        return json.loads(content)

    def load_query(self, query_dir, query_name):
        query_path = Path(str(QUERIES_DIR / query_dir / f"{query_name}.graphql"))
        if not query_path.exists():
            return None

        query = query_path.read_text()

        return query

    def load_variables(self, query_dir, query_name):
        variables = Path(str(QUERIES_DIR / query_dir / f"{query_name}.variables.json"))
        if not variables.exists():
            return None

        content = variables.read_text()

        # return actual json
        return json.loads(content)

    def dict_nested_keys(self, d):
        return list(flatten(d).keys())

    def prepare_db(self):
        self.run_async(fastplatform.connect())

        # Clear db from all previous test values
        self.run_async(fastplatform.execute(gql(self.clear_dbs_query)))

        # Important: give events triggered by deletion the time to be fully processed
        time.sleep(2)

        self.run_async(fastplatform.execute(gql(self.setup_query)))

        time.sleep(2)

        self.run_async(fastplatform.close())

    def test_queries(self):
        self.prepare_db()

        with TestClient(app) as client:

            for query_dir, query_name in self.get_tested_queries():
                with self.subTest(query_name=query_name):
                    api_return_value = self.load_api_return_value(query_dir, query_name)
                    expected = self.load_expected(query_dir, query_name)
                    expected_struct = self.load_expected_struct(query_dir, query_name)
                    query = self.load_query(query_dir, query_name)
                    headers = self.load_headers(query_dir, query_name)
                    variables = self.load_variables(query_dir, query_name)

                    if api_return_value:
                        opw_client.http_client = MockApiClient(
                            status_code=200, data=api_return_value
                        )

                    if RUN_ONLY and not (query_dir == RUN_ONLY):
                        continue

                    print("-" * 60)
                    print(f"QUERY:    {query_name}")
                    print("-" * 60)

                    QueriesTest.ran += 1

                    response = client.post(
                        "/graphql",
                        headers=headers,
                        json={"query": query, "variables": variables},
                    )

                    # Keep for easier debug
                    # print("Response formatted: ", json.dumps(response.json()))
                    # print()
                    # print(
                    #     "Expected:           " + json.dumps(expected)
                    #     if expected
                    #     else "Expected struct:     " + json.dumps(expected_struct),
                    # )
                    # print()
                    # print()

                    # Assert results are identical
                    if expected:
                        TestCase().assertDictEqual(response.json(), expected)
                    elif expected_struct:
                        TestCase().assertListEqual(
                            self.dict_nested_keys(response.json()),
                            self.dict_nested_keys(expected_struct),
                        )

                    QueriesTest.succeeded += 1
                    print("--> OK")
                    print()
