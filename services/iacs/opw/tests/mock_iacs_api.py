from httpx import HTTPStatusError


class MockApiClient:
    status_code: int = 200
    data: dict = {}

    def __init__(self, status_code=200, data={}) -> None:
        self.status_code = status_code
        self.data = data

    async def get(self, url: str, headers: dict, timeout: int):
        return MockApiResponse(self.status_code, self.data)

    async def aclose(self):
        pass


class MockApiResponse:
    status_code: int
    data: dict

    def __init__(self, status_code, data) -> None:
        self.status_code = status_code
        self.data = data

    def raise_for_status(self):
        if self.status_code == 200:
            return

        class MockHttpxResponse:
            status_code: int

        mock_response = MockHttpxResponse()
        mock_response.status_code = self.status_code
        raise HTTPStatusError("message", request=None, response=mock_response)

    def json(self):
        return self.data
