from datetime import date, datetime
from unittest import IsolatedAsyncioTestCase, mock
import copy
import json

from gql import gql

from app.api.errors import (
    GraphQLIacsObjectNotFoundError,
    GraphQLIacsRemoteError,
    GraphQLIacsServiceError,
    GraphQLIacsUserForbiddenError,
)
from app.api.lib.opw import opw_client
from app.api.mutations.sync_holding import SyncHolding, logger as sync_holding_logger
from app.api.mutations.sync_holdings import SyncHoldings, logger as sync_holdings_logger
from app.api.mutations.insert_demo_holding import (
    InsertDemoHolding,
    logger as insert_demo_logger,
)
from app.db.graphql_clients import fastplatform, GraphQLClient
from app.settings import config

from tests import MockApiClient

from . import mock_data as md

GRAPHQL_ERRORS_BY_HTTPX_STATUS_CODE = [
    ("400", GraphQLIacsServiceError),
    ("401", GraphQLIacsServiceError),
    ("403", GraphQLIacsUserForbiddenError),
    ("404", GraphQLIacsObjectNotFoundError),
    ("500", GraphQLIacsRemoteError),
]

DISABLE_SERVICE_LOGS = True
SERVICE_LOGS_DISABLED = ["debug", "info", "warning", "error", "exception"]


class MockGraphQLSession:
    data: dict

    async def execute(self, *args, **kwargs):
        return self.data


class IACSTest(IsolatedAsyncioTestCase):
    mock_api_client = MockApiClient()
    mock_graphql_session = MockGraphQLSession()

    def setUp(self) -> None:

        # For cleaner tests stdout
        if DISABLE_SERVICE_LOGS:
            for log_disabled in SERVICE_LOGS_DISABLED:
                setattr(sync_holding_logger, log_disabled, mock.MagicMock())
                setattr(sync_holdings_logger, log_disabled, mock.MagicMock())
                setattr(insert_demo_logger, log_disabled, mock.MagicMock())

        # Replace iacs_client.http_client with our mock
        opw_client.http_client = self.mock_api_client

        # Replace fastplatform.session with our mock
        setattr(fastplatform, "session", self.mock_graphql_session)

        return super().setUp()

    # Testing SyncHoldings, get_producteurs sequence:
    # upserting the holding the user is allowed to access
    async def test_holdings_get_producteurs_nominal_case(self):
        self.mock_api_client.status_code = 200
        self.mock_api_client.data = md.API_GET_PRODUCTEURS_MOCK_DATA

        # Essentially testing it doesn't raise
        data = await SyncHoldings.get_producteurs(md.USER_ID)

        self.assertIsInstance(data, list)
        self.assertListEqual(data, md.SYNC_HOLDINGS_GET_PRODUCTEURS_EXPECTED)

        # Essentially testing it doesn't raise
        upsert = SyncHoldings.build_holding_upsert(md.USER_ID, data)
        self.assertIsInstance(upsert, list)
        self.assertListEqual(upsert, md.SYNC_HOLDINGS_BUILD_HOLDING_UPSERT_EXPECTED)

    def test_holdings_get_x_hasura_user_id_raises_service_error(self):
        with self.assertRaises(GraphQLIacsServiceError):
            # Simulate a request error by sending None
            SyncHoldings.get_x_hasura_user_id(request=None)

    async def test_holdings_httpx_status_codes_raise_correct_graphql_error(self):

        for status_code, graphql_error in GRAPHQL_ERRORS_BY_HTTPX_STATUS_CODE:
            with self.subTest(status_code=status_code):

                self.mock_api_client.status_code = int(status_code)
                with self.assertRaises(graphql_error):
                    await SyncHoldings.get_producteurs(md.USER_ID)

    def test_holdings_build_holdings_upsert_error_raises_service_error(
        self,
    ):
        expected_keys = [
            "id",  # only key used at this stage...
        ]

        for expected_key in expected_keys:
            with self.subTest(expected_key=expected_key):
                # Convert to string for easy corruption
                unexpected = json.dumps(
                    md.SYNC_HOLDINGS_GET_PRODUCTEURS_EXPECTED
                ).replace(expected_key, "unexpected_key")

                # Convert back to dict
                unexpected = json.loads(unexpected)

                with self.assertRaises(GraphQLIacsServiceError):
                    SyncHoldings.build_holding_upsert(md.USER_ID, unexpected)

    # Testing SyncHolding, get_plots sequence
    # upserting the plots of one specific holding the user requested
    async def test_holding_get_parcelles_nominal_case(self):
        class MockSyncHoldingRequest:
            headers: dict

        mock_request = MockSyncHoldingRequest()
        mock_request.headers = {
            "X-Hasura-User-Id": md.USER_ID,
            "X-Hasura-Role": md.USER_ROLE,
        }

        # Checking nominal case doesn't raise
        SyncHolding.get_x_hasura_user_id_and_role(mock_request)

        self.mock_graphql_session.data = {"holding": ["some holding"]}

        # Again, checking nominal case doesn't raise
        # which both implies holding exists in api and user is allowed.
        await SyncHolding.query_holding_info(
            md.HOLDING_ID,
            md.USER_ID,
            md.USER_ROLE,
        )

        self.mock_api_client.status_code = 200
        self.mock_api_client.data = md.API_GET_PARCELLES_MOCK_DATA

        data = await SyncHolding.get_parcelles(md.USER_ID, md.HOLDING_ID)
        self.assertIsInstance(data, list)
        self.assertListEqual(data, md.SYNC_HOLDING_GET_PARCELLES_EXPECTED)

        # Checking nominal case doesn't raise
        messages = []
        plots = SyncHolding.format_plots(data, messages, {})
        self.assertListEqual(messages, [])

        # Mock fastplatform response for plant_varieties
        self.mock_graphql_session.data = md.PLANT_VARIETY_RETURN_VALUES

        # Again, checking nominal case doesn't raise
        await SyncHolding.check_plant_varieties_exist(plots, messages, {})
        self.assertListEqual(messages, [])

        self.assertIsInstance(plots, list)
        self.assertListEqual(plots, md.PLOTS_OK)

        holding_campaigns = SyncHolding.build_holding_campaigns(
            md.HOLDING_ID, md.CAMPAIGN_ID, plots
        )
        self.assertIsInstance(holding_campaigns, list)
        self.assertListEqual(
            holding_campaigns,
            md.SYNC_HOLDING_BUILD_HOLDING_CAMPAIGNS_EXPECTED,
        )

    def test_holding_get_x_hasura_user_id_and_role_raises_service_error(self):
        with self.assertRaises(GraphQLIacsServiceError):
            # Simulate a request error by sending None
            SyncHolding.get_x_hasura_user_id_and_role(request=None)

    async def test_holding_hasura_return_values_triggering_service_error(self):
        expected_key = "holding"
        hasura_return_values_triggering_service_error = [
            None,
            object(),
            [],
            {},
            {"unexpected_key": "some_value"},
            {expected_key: None},
            {expected_key: {}},
        ]

        for return_value in hasura_return_values_triggering_service_error:
            with self.subTest(return_value=return_value):
                with self.assertRaises(GraphQLIacsServiceError):
                    self.mock_graphql_session.data = return_value
                    await SyncHolding.query_holding_info(
                        md.HOLDING_ID,
                        md.USER_ID,
                        md.USER_ROLE,
                    )

    async def test_holding_hasura_empty_return_value_triggers_not_found_error(self):

        with self.assertRaises(GraphQLIacsObjectNotFoundError):
            self.mock_graphql_session.data = {"holding": []}
            await SyncHolding.query_holding_info(
                md.HOLDING_ID,
                md.USER_ID,
                md.USER_ROLE,
            )

    async def test_holding_httpx_status_codes_raise_correct_graphql_error(self):

        for status_code, graphql_error in GRAPHQL_ERRORS_BY_HTTPX_STATUS_CODE:
            with self.subTest(status_code=status_code):

                self.mock_api_client.status_code = int(status_code)
                with self.assertRaises(graphql_error):
                    await SyncHolding.get_parcelles(md.USER_ID, md.HOLDING_ID)

    def test_holding_all_plots_unparsable_does_not_raise(self):
        messages = []
        plots = SyncHolding.format_plots(
            md.PARCELLES_ALL_UNPARSABLE_GEOMETRIE, messages, {}
        )
        self.assertIsInstance(plots, list)
        self.assertEqual(len(plots), 0)
        self.assertEqual(len(messages), 1)

    def test_holding_format_plots_incomplete_does_not_raise(self):
        messages = []
        plots = SyncHolding.format_plots(
            md.PARCELLES_SOME_UNPARSABLE_GEOMETRIE, messages, {}
        )
        self.assertIsInstance(plots, list)
        self.assertEqual(len(plots), 3)
        self.assertEqual(len(messages), 1)

    def test_holding_format_plots_filters_out_plot_ids_longer_than_256_chars(self):
        valid_plot_id_count = len(md.SYNC_HOLDING_GET_PARCELLES_EXPECTED)

        plots_with_one_plot_id_too_long = copy.deepcopy(
            md.SYNC_HOLDING_GET_PARCELLES_EXPECTED
        )
        plots_with_one_plot_id_too_long[0]["id"] = "x" * 257
        formatted = SyncHolding.format_plots(plots_with_one_plot_id_too_long, [], {})

        # Assert one plot is not inserted
        self.assertEqual(len(formatted), valid_plot_id_count - 1)

    async def test_sync_holding_some_unknown_cultures(self):
        messages = []

        # Mock fastplatform response for plant_varieties
        self.mock_graphql_session.data = md.PLANT_VARIETY_RETURN_VALUES

        # Again, checking nominal case doesn't raise
        await SyncHolding.check_plant_varieties_exist(
            md.PLOTS_WITH_UNKNOWN_CULTURES_RETURN_VALUE, messages, {}
        )
        self.assertEqual(len(messages), 1)

    def test_holding_build_holding_campaign_raises_service_error(self):
        nok_data_plot = {
            "wrong_geometrie_key": md.PLOTS_WRONG_GEOMETRY_KEY,
            "wrong_authority_id": md.PLOTS_WRONG_AUTHORITY_ID_KEY,
            "wrong_name": md.PLOTS_WRONG_NAME_KEY,
        }

        for nok_data_type, plots in nok_data_plot.items():
            with self.subTest(nok_data_type=nok_data_type):
                with self.assertRaises(GraphQLIacsServiceError):
                    SyncHolding.build_holding_campaigns(
                        md.HOLDING_ID, md.CAMPAIGN_ID, plots
                    )

    def test_demo_holding_get_x_hasura_user_id_and_role_raises_service_error(self):
        with self.assertRaises(GraphQLIacsServiceError):
            # Simulate a request error by sending None
            InsertDemoHolding.get_x_hasura_user_id(request=None)

    async def test_demo_holding_nominal(self):

        # We are "testing" demo holding using a real graphql client
        fp_client = GraphQLClient(
            name="fastplatform",
            url=config.API_GATEWAY_FASTPLATFORM_URL,
            headers={
                "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                "Content-type": "application/json",
            },
        )
        await fp_client.connect()

        # Create one test user

        try:
            query = md.MUTATION_INSERT_USER.replace("{{ id }}", "username")
            query = query.replace("{{ username }}", "username")
            query = query.replace("{{ date }}", str(date.today()))
            await fp_client.execute(gql(query))
        except Exception as ex:
            # user already exists
            pass

        try:
            query = md.MUTATION_INSERT_CAMPAIGN.replace("{{ id }}", md.CAMPAIGN_ID)
            query = query.replace("{{ name }}", "2020-2021")
            query = query.replace("{{ startdate }}", str(datetime(2020, 1, 1, 0, 0)))
            query = query.replace("{{ enddate }}", str(datetime(2021, 1, 1, 0, 0)))
            await fp_client.execute(gql(query))
        except Exception as ex:
            # campaign already exists
            pass

        # Upsert demo holding
        holding = InsertDemoHolding.create_mutation(md.CAMPAIGN_ID, "username")
        result = await InsertDemoHolding.insert_demo_holding(fp_client, holding)

        self.assertIsInstance(result, dict)
        self.assertDictEqual(result, md.EXPECTED_INSERT_DEMO_HOLDING_RESULT)

        await fp_client.close()
