USER_ID = "USER_ID"
USER_ROLE = "farmer"
HOLDING_ID = "HOLDING_ID"
CAMPAIGN_ID = 2021

# producteurs
SYNC_HOLDINGS_BUILD_HOLDING_UPSERT_EXPECTED = [
    {
        "id": "id1",
        "name": "id1",
        "user_related_parties": {
            "data": [{"role": "farmer", "user_id": USER_ID}],
            "on_conflict": {
                "constraint": "user_related_party_holding_id_user_id_unique",
                "update_columns": ["role"],
            },
        },
    },
    {
        "id": "id2",
        "name": "id2",
        "user_related_parties": {
            "data": [{"role": "farmer", "user_id": USER_ID}],
            "on_conflict": {
                "constraint": "user_related_party_holding_id_user_id_unique",
                "update_columns": ["role"],
            },
        },
    },
    {
        "id": "id3",
        "name": "id3",
        "user_related_parties": {
            "data": [{"role": "farmer", "user_id": USER_ID}],
            "on_conflict": {
                "constraint": "user_related_party_holding_id_user_id_unique",
                "update_columns": ["role"],
            },
        },
    },
]
API_GET_PRODUCTEURS_MOCK_DATA = [
    {"id": "id1", "role": None},
    {"id": "id2", "role": None},
    {"id": "id3", "role": None},
]
SYNC_HOLDINGS_GET_PRODUCTEURS_EXPECTED = API_GET_PRODUCTEURS_MOCK_DATA

SYNC_HOLDING_GET_PARCELLES_EXPECTED = [
    {
        "id": "plot_id_1",
        "nom": "parcelle 1",
        "geometrie": "POLYGON ((4.59751716628341 50.2402032261216, 4.59731813738496 50.2401873926804, 4.59724752498555 50.240176811343, 4.59751716628341 50.2402032261216))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_2",
        "nom": "parcelle 2",
        "geometrie": "POLYGON ((4.97362572063101 50.463454898456, 4.97385738157506 50.463493367196, 4.97385719965854 50.4634935490758, 4.97362572063101 50.463454898456))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_3",
        "nom": "parcelle 3",
        "geometrie": "POLYGON ((4.97342477230525 50.462719638658, 4.97352381496139 50.4627550839391, 4.97358982432083 50.4627772157936, 4.97342477230525 50.462719638658))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_4",
        "nom": "parcelle 4",
        "geometrie": "POLYGON ((4.97307155099715 50.4628527367242, 4.9728892062717 50.4626909612133, 4.97247912787946 50.462562735244, 4.97307155099715 50.4628527367242))",
        "cultureCode": "201",
    },
]

SYNC_HOLDING_BUILD_HOLDING_CAMPAIGNS_EXPECTED = [
    {
        "campaign_id": 2021,
        "holding_id": "HOLDING_ID",
        "sites": {
            "data": [
                {
                    "authority_id": "HOLDING_ID",
                    "name": "HOLDING_ID",
                    "plots": {
                        "data": [
                            {
                                "authority_id": "plot_id_1",
                                "name": "parcelle 1",
                                "geometry": {
                                    "type": "MultiPolygon",
                                    "coordinates": [
                                        (
                                            (
                                                (4.59751716628341, 50.2402032261216),
                                                (4.59731813738496, 50.2401873926804),
                                                (4.59724752498555, 50.240176811343),
                                                (4.59751716628341, 50.2402032261216),
                                            ),
                                        )
                                    ],
                                    "crs": {
                                        "type": "name",
                                        "properties": {"name": "EPSG:4258"},
                                    },
                                },
                                "plot_plant_varieties": {
                                    "data": [{"plant_variety_id": "201"}],
                                    "on_conflict": {
                                        "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                        "update_columns": [
                                            "plot_id",
                                            "plant_variety_id",
                                        ],
                                    },
                                },
                            },
                            {
                                "authority_id": "plot_id_2",
                                "name": "parcelle 2",
                                "geometry": {
                                    "type": "MultiPolygon",
                                    "coordinates": [
                                        (
                                            (
                                                (4.97362572063101, 50.463454898456),
                                                (4.97385738157506, 50.463493367196),
                                                (4.97385719965854, 50.4634935490758),
                                                (4.97362572063101, 50.463454898456),
                                            ),
                                        )
                                    ],
                                    "crs": {
                                        "type": "name",
                                        "properties": {"name": "EPSG:4258"},
                                    },
                                },
                                "plot_plant_varieties": {
                                    "data": [{"plant_variety_id": "201"}],
                                    "on_conflict": {
                                        "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                        "update_columns": [
                                            "plot_id",
                                            "plant_variety_id",
                                        ],
                                    },
                                },
                            },
                            {
                                "authority_id": "plot_id_3",
                                "name": "parcelle 3",
                                "geometry": {
                                    "type": "MultiPolygon",
                                    "coordinates": [
                                        (
                                            (
                                                (4.97342477230525, 50.462719638658),
                                                (4.97352381496139, 50.4627550839391),
                                                (4.97358982432083, 50.4627772157936),
                                                (4.97342477230525, 50.462719638658),
                                            ),
                                        )
                                    ],
                                    "crs": {
                                        "type": "name",
                                        "properties": {"name": "EPSG:4258"},
                                    },
                                },
                                "plot_plant_varieties": {
                                    "data": [{"plant_variety_id": "201"}],
                                    "on_conflict": {
                                        "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                        "update_columns": [
                                            "plot_id",
                                            "plant_variety_id",
                                        ],
                                    },
                                },
                            },
                            {
                                "authority_id": "plot_id_4",
                                "name": "parcelle 4",
                                "geometry": {
                                    "type": "MultiPolygon",
                                    "coordinates": [
                                        (
                                            (
                                                (4.97307155099715, 50.4628527367242),
                                                (4.9728892062717, 50.4626909612133),
                                                (4.97247912787946, 50.462562735244),
                                                (4.97307155099715, 50.4628527367242),
                                            ),
                                        )
                                    ],
                                    "crs": {
                                        "type": "name",
                                        "properties": {"name": "EPSG:4258"},
                                    },
                                },
                                "plot_plant_varieties": {
                                    "data": [{"plant_variety_id": "201"}],
                                    "on_conflict": {
                                        "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                        "update_columns": [
                                            "plot_id",
                                            "plant_variety_id",
                                        ],
                                    },
                                },
                            },
                        ],
                        "on_conflict": {
                            "constraint": "plot_authority_id_site_id_unique",
                            "update_columns": [
                                "authority_id",
                                "site_id",
                                "name",
                                "geometry",
                            ],
                        },
                    },
                }
            ],
            "on_conflict": {
                "constraint": "site_authority_id_holding_campaign_id_unique",
                "update_columns": ["authority_id", "holding_campaign_id", "name"],
            },
        },
    }
]

PRODUCTEURS_NOK = [
    {"wrong_key": "id1", "another_wrong_key": None},
]

# parcelles
API_GET_PARCELLES_MOCK_DATA = [
    {
        "id": "plot_id_1",
        "nom": "parcelle 1",
        "geometrie": "POLYGON ((4.59751716628341 50.2402032261216, 4.59731813738496 50.2401873926804, 4.59724752498555 50.240176811343, 4.59751716628341 50.2402032261216))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_2",
        "nom": "parcelle 2",
        "geometrie": "POLYGON ((4.97362572063101 50.463454898456, 4.97385738157506 50.463493367196, 4.97385719965854 50.4634935490758, 4.97362572063101 50.463454898456))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_3",
        "nom": "parcelle 3",
        "geometrie": "POLYGON ((4.97342477230525 50.462719638658, 4.97352381496139 50.4627550839391, 4.97358982432083 50.4627772157936, 4.97342477230525 50.462719638658))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_4",
        "nom": "parcelle 4",
        "geometrie": "POLYGON ((4.97307155099715 50.4628527367242, 4.9728892062717 50.4626909612133, 4.97247912787946 50.462562735244, 4.97307155099715 50.4628527367242))",
        "cultureCode": "201",
    },
]

PARCELLES_WRONG_ID_KEY = [
    {
        "wrong_key": "plot_id_1",
        "nom": "parcelle 1",
        "geometrie": "POLYGON ((4.97307155099715 50.4628527367242, 4.9728892062717 50.4626909612133, 4.97247912787946 50.462562735244, 4.97307155099715 50.4628527367242))",
        "cultureCode": "201",
    },
]

PARCELLES_WRONG_GEOMETRIE_KEY = [
    {
        "id": "plot_id_1",
        "nom": "parcelle 1",
        "wrong_key": "POLYGON ((4.97307155099715 50.4628527367242, 4.9728892062717 50.4626909612133, 4.97247912787946 50.462562735244, 4.97307155099715 50.4628527367242))",
        "cultureCode": "201",
    },
]

# IllegalArgumentException: Points of LinearRing do not form a closed linestring
PARCELLES_ALL_UNPARSABLE_GEOMETRIE = [
    {
        "id": "plot_id_1",
        "nom": "parcelle 1",
        "geometrie": "POLYGON ((4.59751716628341 50.2402032261216, 4.59731813738496 50.2401873926804, 4.59724752498555 50.240176811343))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_2",
        "nom": "parcelle 2",
        "geometrie": "POLYGON ((4.97362572063101 50.463454898456, 4.97385738157506 50.463493367196, 4.97385719965854 50.4634935490758))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_3",
        "nom": "parcelle 3",
        "geometrie": "POLYGON ((4.97342477230525 50.462719638658, 4.97352381496139 50.4627550839391, 4.97358982432083 50.4627772157936))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_4",
        "nom": "parcelle 4",
        "geometrie": "POLYGON ((4.97307155099715 50.4628527367242, 4.9728892062717 50.4626909612133, 4.97247912787946 50.462562735244))",
        "cultureCode": "201",
    },
]

PARCELLES_SOME_UNPARSABLE_GEOMETRIE = [
    {
        "id": "plot_id_1",
        "nom": "parcelle 1",
        # IllegalArgumentException: Points of LinearRing do not form a closed linestring
        "geometrie": "POLYGON ((4.59751716628341 50.2402032261216, 4.59731813738496 50.2401873926804, 4.59724752498555 50.240176811343))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_2",
        "nom": "parcelle 2",
        "geometrie": "POLYGON ((4.97362572063101 50.463454898456, 4.97385738157506 50.463493367196, 4.97385719965854 50.4634935490758, 4.97362572063101 50.463454898456))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_3",
        "nom": "parcelle 3",
        "geometrie": "POLYGON ((4.97342477230525 50.462719638658, 4.97352381496139 50.4627550839391, 4.97358982432083 50.4627772157936, 4.97342477230525 50.462719638658))",
        "cultureCode": "201",
    },
    {
        "id": "plot_id_4",
        "nom": "parcelle 4",
        "geometrie": "POLYGON ((4.97307155099715 50.4628527367242, 4.9728892062717 50.4626909612133, 4.97247912787946 50.462562735244, 4.97307155099715 50.4628527367242))",
        "cultureCode": "201",
    },
]


# plots
PLOTS_OK = [
    {
        "authority_id": "plot_id_1",
        "name": "parcelle 1",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.59751716628341, 50.2402032261216),
                        (4.59731813738496, 50.2401873926804),
                        (4.59724752498555, 50.240176811343),
                        (4.59751716628341, 50.2402032261216),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": "201",
    },
    {
        "authority_id": "plot_id_2",
        "name": "parcelle 2",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.97362572063101, 50.463454898456),
                        (4.97385738157506, 50.463493367196),
                        (4.97385719965854, 50.4634935490758),
                        (4.97362572063101, 50.463454898456),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": "201",
    },
    {
        "authority_id": "plot_id_3",
        "name": "parcelle 3",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.97342477230525, 50.462719638658),
                        (4.97352381496139, 50.4627550839391),
                        (4.97358982432083, 50.4627772157936),
                        (4.97342477230525, 50.462719638658),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": "201",
    },
    {
        "authority_id": "plot_id_4",
        "name": "parcelle 4",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.97307155099715, 50.4628527367242),
                        (4.9728892062717, 50.4626909612133),
                        (4.97247912787946, 50.462562735244),
                        (4.97307155099715, 50.4628527367242),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": "201",
    },
]

PLOTS_WITH_UNKNOWN_CULTURES_RETURN_VALUE = [
    {
        "authority_id": "plot_id_1",
        "name": "parcelle 1",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.59751716628341, 50.2402032261216),
                        (4.59731813738496, 50.2401873926804),
                        (4.59724752498555, 50.240176811343),
                        (4.59751716628341, 50.2402032261216),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": 999999999,
    },
    {
        "authority_id": "plot_id_2",
        "name": "parcelle 2",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.97362572063101, 50.463454898456),
                        (4.97385738157506, 50.463493367196),
                        (4.97385719965854, 50.4634935490758),
                        (4.97362572063101, 50.463454898456),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": 202,
    },
    {
        "authority_id": "plot_id_3",
        "name": "parcelle 3",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.97342477230525, 50.462719638658),
                        (4.97352381496139, 50.4627550839391),
                        (4.97358982432083, 50.4627772157936),
                        (4.97342477230525, 50.462719638658),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": 201,
    },
    {
        "authority_id": "plot_id_4",
        "name": "parcelle 4",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.97307155099715, 50.4628527367242),
                        (4.9728892062717, 50.4626909612133),
                        (4.97247912787946, 50.462562735244),
                        (4.97307155099715, 50.4628527367242),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
        "plant_variety_id": 202,
    },
]

PLOTS_WRONG_AUTHORITY_ID_KEY = [
    {
        "wrong_key": "plot_id_1",
        "name": "parcelle 1",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.59751716628341, 50.2402032261216),
                        (4.59731813738496, 50.2401873926804),
                        (4.59724752498555, 50.240176811343),
                        (4.59751716628341, 50.2402032261216),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
    },
]

PLOTS_WRONG_NAME_KEY = [
    {
        "authority_id": "plot_id_1",
        "wrong_key": "parcelle 1",
        "geometry": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.59751716628341, 50.2402032261216),
                        (4.59731813738496, 50.2401873926804),
                        (4.59724752498555, 50.240176811343),
                        (4.59751716628341, 50.2402032261216),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
    },
]

PLOTS_WRONG_GEOMETRY_KEY = [
    {
        "authority_id": "plot_id_1",
        "name": "parcelle 1",
        "wrong_key": {
            "type": "MultiPolygon",
            "coordinates": [
                (
                    (
                        (4.59751716628341, 50.2402032261216),
                        (4.59731813738496, 50.2401873926804),
                        (4.59724752498555, 50.240176811343),
                        (4.59751716628341, 50.2402032261216),
                    ),
                )
            ],
            "crs": {"type": "name", "properties": {"name": "EPSG:4258"}},
        },
    },
]

# plants
PLANT_VARIETY_RETURN_VALUES = {
    "plant_variety": [
        {
            "id": "201",
            "name": "-",
            "plant_species": {"id": "201", "name": "Mais ensilage"},
        },
        {
            "id": "202",
            "name": "-",
            "plant_species": {"id": "202", "name": "Mais grain"},
        },
    ]
}

# demo_holding
EXPECTED_INSERT_DEMO_HOLDING_RESULT = {"insert_holding_one": {"id": "DEMO-username"}}

MUTATION_INSERT_USER = """
mutation {
    insert_user(
        objects: [
        {
            id: "{{ id }}"
            username: "{{ username }}"
            password: "azerty1234"
            date_joined: "{{ date }}"
        }
        ]
    ) {
        returning {
        id
        }
    }
}
"""

MUTATION_INSERT_CAMPAIGN = """
            mutation {
                insert_campaign(
                    objects: [
                    {
                        id: {{ id }}
                        name: "{{ name }}"
                        is_current: false
                        start_at: "{{ startdate }}"
                        end_at: "{{ enddate }}"
                    }
                    ]
                ) {
                    returning {
                    id
                    }
                }
            }
        """
