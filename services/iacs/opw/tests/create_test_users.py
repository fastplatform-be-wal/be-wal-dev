from datetime import date
import argparse
import asyncio
import logging

from gql import gql
from app.db.graphql_clients import fastplatform
from .unit_testing import mock_data as md

logger = logging.getLogger(__name__)
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--ids", nargs="+")


async def create_users():
    await fastplatform.connect()

    args = parser.parse_args()
    user_ids_to_create = args.ids

    mutation = md.MUTATION_INSERT_USER
    created = 0
    for user in user_ids_to_create:
        query = mutation.replace("{{ id }}", user)
        query = query.replace("{{ username }}", user)
        query = query.replace("{{ date }}", str(date.today()))

        try:
            await fastplatform.execute(gql(query))
            created += 1
        except Exception as ex:
            print(
                "User(s) could not be imported.\nMutation: %s \nRaised %s", query, ex
            )
            continue

    print(f"{created} user(s) created")
    await fastplatform.close()


if __name__ == "__main__":
    asyncio.run(create_users())
