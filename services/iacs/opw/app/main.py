import logging

import graphene
import uvicorn
from fastapi import FastAPI
from starlette_graphene3 import GraphQLApp

from app.api.lib.opw import opw_client
from app.api.mutation import Mutation
from app.api.query import Query
from app.db.graphql_clients import fastplatform
from app.settings import config
from app.tracing import Tracing

# FastAPI
app = FastAPI()

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, mutation=Mutation, auto_camelcase=False)
    ),
)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():

    """On app startup"""
    # Debugging instrumentation
    if config.REMOTE_DEBUG:
        import debugpy
        debugpy.listen(5678)

    await fastplatform.connect()

    opw_client.create_http_client()
    opw_client.refresh_access_token()


@app.on_event("shutdown")
async def shutdown():
    """On app shutdown"""
    await fastplatform.close()
    await opw_client.close_http_client()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7777)
