import os
import sys
from pathlib import Path
from typing import Dict, Union

from pydantic import BaseSettings


class Settings(BaseSettings):

    API_DIR: Path = Path(__file__).parent / "api"

    API_GATEWAY_FASTPLATFORM_URL: str
    # 2 minutes, there can be some heavy farms to write to Hasura
    API_GATEWAY_TIMEOUT: int = 120
    API_GATEWAY_SERVICE_KEY: str

    CULTURE_CODES_IN_MESSAGE_TRUNCATE_AFTER: int = 3
    NOT_ALL_PLOTS_INSERTED_MESSAGE: str = (
        "{PLOT_COUNT} parcelle(s) n'ont pas été ingérées"
    )
    OPW_API_GET_PARCELLES_URL: str = "{OPW_API_ROOT}/producteurs/{PRODUCTEUR_ID}/numero-national/{NUMERO_NATIONAL}/parcelles"
    OPW_API_GET_PRODUCTEURS_URL: str = (
        "{OPW_API_ROOT}/producteurs/numero-national/{NUMERO_NATIONAL}"
    )
    OPW_API_ROOT: str = "https://agriculture.valid.wallonie.be/opw/gateway/fast/api/v1"
    OPW_JWT_SIGNING_ALGORITHM: str = "HS256"
    OPW_KID: str
    OPW_SECRET_KEY: str
    OPW_TIMEOUT: int = 60
    OPW_TOKEN_VALIDITY_PERIOD: int = 3600
    OPW_GEOMETRY_EPSG: str = "4326"
    PLOTS_IN_MESSAGE_TRUNCATE_AFTER: int = 3
    SOME_PLOTS_WITH_CULTURE_CODE_MISSING: str = (
        "{PLOT_COUNT} parcelle(s) ont un code culture non-reconnu"
    )

    IACS_DISPLAY_NAME: str = "Organisme Payeur de Wallonie"
    IACS_SHORT_NAME: str = "OPW"
    IACS_WEBSITE_URL: str = (
        "https://agriculture.wallonie.be/organisme-payeur-de-wallonie-et-coordination"
    )

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0
    OPENTELEMETRY_SERVICE_NAME: str = "iacs-opw"

    REMOTE_DEBUG: bool = False

    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "fastapi": {"propagate": True},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }


config = Settings()
