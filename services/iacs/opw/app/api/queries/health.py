import graphene


class HealthQuery(graphene.ObjectType):

    iacs_opw__healthz = graphene.String(default_value="ok")
