import graphene
from app.settings import config


class NameQuery(graphene.ObjectType):

    iacs_display_name = graphene.String(default_value=config.IACS_DISPLAY_NAME)
    iacs_short_name = graphene.String(default_value=config.IACS_SHORT_NAME)
    iacs_website_url = graphene.String(default_value=config.IACS_WEBSITE_URL)
