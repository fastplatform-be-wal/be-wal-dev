import json
from graphql import GraphQLError
from app.settings import config


class GraphQLIacsUserUnauthenticatedError(GraphQLError):
    """
    - Definition: the user is not authenticated to query the requested data (eg token expired)
    - Expected app UX:  "Log in again, etc"
    - Extra params in GraphQLError.extensions (see __init__)

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
        validate, or execute phases of performing a GraphQL operation.
    """

    message = "IACS_USER_UNAUTHENTICATED"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLIacsUserUnauthenticatedError.message, *args, **kwargs)

    def __str__(self):
        return json.dumps(self.__dict__)


class GraphQLIacsUserForbiddenError(GraphQLError):
    """
    - Definition: the user is not allowed to query the requested data
    - Expected app UX:
        "You are not allowed to request this data.
        If you think yiu should then connect to your PA portal
        and ensure you have sufficient rights"
    - Extra params in GraphQLError.extensions (see __init__)

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
        validate, or execute phases of performing a GraphQL operation.
    """

    message = "IACS_USER_FORBIDDEN"

    def __init__(self, *args, **kwargs):
        keys = ["paying_agency_display_name", "paying_agency_url"]
        if all(key in kwargs for key in keys):
            extensions = {key: kwargs[key] for key in kwargs if key in keys}
            # Remove keys from kwargs
            kwargs = {key: kwargs[key] for key in kwargs if key not in keys}
        else:
            extensions = {
                "paying_agency_display_name": config.IACS_DISPLAY_NAME,
                "paying_agency_url": config.IACS_WEBSITE_URL,
            }

        super().__init__(GraphQLIacsUserForbiddenError.message, *args, **kwargs)
        self.extensions = extensions

    def __str__(self):
        return json.dumps(self.__dict__)


class GraphQLIacsObjectNotFoundError(GraphQLError):
    """
    - Definition: the user, the holding or the holding_campaign does not exist
    - Expected app UX:
        "Either you (<user_id>) are not registered in the IACS system
        or holding <holding_id> does not exist in the IACS system"
    - Extra params in GraphQLError.extensions (see __init__)

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
        validate, or execute phases of performing a GraphQL operation.
    """

    message = "IACS_OBJECT_NOT_FOUND"

    def __init__(self, *args, **kwargs):
        keys = ["paying_agency_display_name", "paying_agency_url"]
        if all(key in kwargs for key in keys):
            extensions = {key: kwargs[key] for key in kwargs if key in keys}
            # Remove keys from kwargs
            kwargs = {key: kwargs[key] for key in kwargs if key not in keys}
        else:
            extensions = {
                "paying_agency_display_name": config.IACS_DISPLAY_NAME,
                "paying_agency_url": config.IACS_WEBSITE_URL,
            }

        super().__init__(GraphQLIacsObjectNotFoundError.message, *args, **kwargs)
        self.extensions = extensions

    def __str__(self):
        return json.dumps(self.__dict__)


class GraphQLIacsServiceError(GraphQLError):
    """
    - Definition: an error occurred on the side of the current IACS service
    - Expected app UX: "Try again later"

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
        validate, or execute phases of performing a GraphQL operation.
    """

    message = "IACS_SERVICE_ERROR"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLIacsServiceError.message, *args, **kwargs)

    def __str__(self):
        return json.dumps(self.__dict__)


class GraphQLIacsRemoteError(GraphQLError):
    """
    - Definition: an error occurred on the side of the paying agency API
    - Expected app UX: "Try again later"

    Args:
        GraphQLError: A GraphQLError describes an Error found during the parse,
        validate, or execute phases of performing a GraphQL operation.
    """

    message = "IACS_REMOTE_ERROR"

    def __init__(self, *args, **kwargs):
        super().__init__(GraphQLIacsRemoteError.message, *args, **kwargs)

    def __str__(self):
        return json.dumps(self.__dict__)
