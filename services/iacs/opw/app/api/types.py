import graphene


class IACSMessage(graphene.ObjectType):
    id = graphene.String(required=True)
    title = graphene.String()
    detail = graphene.String()
