import logging

import graphene
from gql import gql
from opentelemetry import trace
from app.api.errors import (
    GraphQLIacsObjectNotFoundError,
    GraphQLIacsServiceError,
)
from app.db.graphql_clients import fastplatform
from app.settings import config


# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class DuplicateHoldingCampaign(graphene.Mutation):
    class Arguments:
        holding_id = graphene.String()
        campaign_from_id = graphene.Int()
        campaign_to_id = graphene.Int()

    holding_campaign_id = graphene.Int()

    async def mutate(self, info, holding_id, campaign_from_id, campaign_to_id):

        request = info.context["request"]

        # Retrieve the holding campaign that we are supposed to duplicate
        with tracer().start_as_current_span("duplicate_holding_campaign_retrieve"):
            print("opw_duplicate_holding_campaign_retrieve")

            query = (
                config.API_DIR / "graphql/query_holding_campaign.graphql"
            ).read_text()
            variables = {"holding_id": holding_id, "campaign_id": campaign_from_id}

            user_id, role = DuplicateHoldingCampaign.get_x_hasura_user_id_and_role(
                request
            )

            # Impersonate the user
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": user_id,
                    "X-Hasura-Role": role,
                }
            }

            response = await fastplatform.execute(
                gql(query), variables, extra_args=extra_args
            )

            # Holding campaign does not exist or the requesting user is not a
            # related party to the holding and hence is not allowed to read it
            if response["holding_campaign"] == []:
                print(
                    "holding_id %s doesn't exist, or user %s not allowed",
                    holding_id,
                    user_id,
                )
                raise GraphQLIacsObjectNotFoundError()

            holding_campaign_from = response["holding_campaign"][0]

        with tracer().start_as_current_span("duplicate_holding_campaigns_upsert"):
            print("opw_duplicate_holding_campaigns_upsert")

            # Build the GraphQL mutation data
            holding_campaigns = [
                {
                    "campaign_id": campaign_to_id,
                    "holding_id": holding_id,
                    "sites": {
                        "data": [
                            {
                                "authority_id": site["authority_id"],
                                "name": site["name"],
                                "plots": {
                                    "data": [
                                        {
                                            "authority_id": plot["authority_id"],
                                            "name": plot["name"],
                                            "geometry": plot["geometry"],
                                            "plot_plant_varieties": {
                                                "data": [
                                                    plot_plant_variety
                                                    for plot_plant_variety in plot[
                                                        "plot_plant_varieties"
                                                    ]
                                                ],
                                                "on_conflict": {
                                                    "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                                    "update_columns": [
                                                        "plot_id",
                                                        "plant_variety_id",
                                                    ],
                                                },
                                            }
                                        }
                                        for plot in site["plots"]
                                    ],
                                    "on_conflict": {
                                        "constraint": "plot_authority_id_site_id_unique",
                                        "update_columns": [
                                            "authority_id",
                                            "site_id",
                                            "name",
                                            "geometry",
                                        ],
                                    },
                                },
                            }
                            for site in holding_campaign_from["sites"]
                        ],
                        "on_conflict": {
                            "constraint": "site_authority_id_holding_campaign_id_unique",
                            "update_columns": [
                                "authority_id",
                                "holding_campaign_id",
                                "name",
                            ],
                        },
                    },
                }
            ]

        with tracer().start_as_current_span("duplicate_holding_campaigns_insert"):
            print("opw_duplicate_holding_campaigns_insert")

            mutation = (
                config.API_DIR / "graphql/mutation_insert_holding_campaigns.graphql"
            ).read_text()
            variables = {"holding_campaigns": holding_campaigns}

            response = await fastplatform.execute(
                gql(mutation),
                variables,
            )

        return DuplicateHoldingCampaign(
            holding_campaign_id=response["insert_holding_campaign"]["returning"][0][
                "id"
            ],
        )

    @staticmethod
    def get_x_hasura_user_id_and_role(request):
        """
        This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header or role
        """
        try:
            return (
                request.headers["X-Hasura-User-Id"],
                request.headers["X-Hasura-Role"],
            )
        except AttributeError as ex:
            print(ex)
            raise GraphQLIacsServiceError() from ex
