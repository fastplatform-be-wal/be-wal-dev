import logging

import graphene
import httpx
import pyproj
import hashlib
from copy import deepcopy

from gql import gql
from httpx import codes
from opentelemetry import trace
from shapely import ops, wkt
from shapely.geometry.multipolygon import MultiPolygon


from app.api.errors import (
    GraphQLIacsObjectNotFoundError,
    GraphQLIacsRemoteError,
    GraphQLIacsServiceError,
    GraphQLIacsUserForbiddenError,
)
from app.api.lib.opw import opw_client
from app.api.types import IACSMessage
from app.db.graphql_clients import fastplatform
from app.settings import config


# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


project_opw_to_4258 = pyproj.Transformer.from_crs(
    pyproj.CRS(f"EPSG:{config.OPW_GEOMETRY_EPSG}"),
    pyproj.CRS("EPSG:4258"),
    always_xy=True,
).transform


class SyncHolding(graphene.Mutation):
    """
    A graphql mutation to upsert the holdings of a user

    """

    class Arguments:
        holding_id = graphene.String(required=True)
        campaign_id = graphene.Int(required=True)
        iacs_year = graphene.Int(required=False)

    plot_ids = graphene.List(graphene.String)
    messages = graphene.List(graphene.NonNull(IACSMessage))

    async def mutate(self, info, holding_id, campaign_id, iacs_year=None):
        """The mutation upserting plots of a specific holding into fast

        Args:
            holding_id (str): The id of the holding whose plots we are trying to upsert
            campaign_id (int): The year of the campaign

        Returns:
            [id]: The list of ids of the plots upserted
            [messages]: The list of messages (if any) to display to user at the end
        """
        request = info.context["request"]

        messages = []
        print("Get Header")
        user_id, role = SyncHolding.get_x_hasura_user_id_and_role(request)
        print("user_id: " + user_id + ", role: " + role)
        message_base_id = {
            "resolver": "SyncHolding",
            "holding_id": holding_id,
            "campaign_id": campaign_id,
            "iacs_year": iacs_year,
            "user_id": user_id,
            "role": role,
        }
        print("Query holding info")
        await SyncHolding.query_holding_info(holding_id, user_id, role)
        print("Get parcelles")
        data = await SyncHolding.get_parcelles(user_id, holding_id)

        plots = SyncHolding.format_plots(data, messages, message_base_id)

        if not plots:
            return SyncHolding(
                plot_ids=[],
                messages=messages,
            )

        await SyncHolding.check_plant_varieties_exist(plots, messages, message_base_id)

        holding_campaigns = SyncHolding.build_holding_campaigns(
            holding_id, campaign_id, plots
        )

        await SyncHolding.insert_holding_campaigns(holding_campaigns)

        return SyncHolding(
            plot_ids=sorted([plot["authority_id"] for plot in plots]),
            messages=messages,
        )

    @staticmethod
    def get_x_hasura_user_id_and_role(request):
        """
        This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header or role
        """
        print("get_x_hasura_user_id_and_role")
        try:
            return (
                request.headers["X-Hasura-User-Id"],
                request.headers["X-Hasura-Role"],
            )
        except AttributeError as ex:
            print(ex)
            raise GraphQLIacsServiceError() from ex

    @staticmethod
    async def query_holding_info(holding_id, user_id, role):
        """
        Retrieve info about the holding we are supposed to gather.
        If user not allowed, query returns no data and method raises,
        which interrupts the process.

        Args:
            holding_id (int): The id of the holding whose plot we are downloading from api
            user_id (str): The id if the user requesting access to holding
            role (str): The role (as per fast) of the user requesting access to holding

        Raises:
            GraphQLIacsServiceError: an error occurred on the side of the current IACS service
            GraphQLIacsObjectNotFoundError: the user or the holding does not exist, or user not allowed
        """

        with tracer().start_as_current_span("query_holding_info"):
            print("opw_query_holding_info")

            query = (config.API_DIR / "graphql/query_holding_by_id.graphql").read_text()
            variables = {"holding_id": holding_id}
            print("config.API_DIR", config.API_DIR)
            # Impersonate the user
            extra_args = {
                "headers": {
                    "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
                    "X-Hasura-User-Id": user_id,
                    "X-Hasura-Role": role,
                }
            }
            print("opw_query_holding_info request")
            response = await fastplatform.execute(
                gql(query), variables, extra_args=extra_args
            )
            print("opw_query_holding_info response", response)
            if (
                response is None
                or not isinstance(response, dict)
                or not "holding" in response
                or not isinstance(response["holding"], list)
            ):
                print("Unexpected hasura response: %s", response)
                print("Unexpected hasura response: %s", response)
                raise GraphQLIacsServiceError

            # Holding does not exist or the requesting user is not a
            # related party to the holding and hence is not allowed to make any
            # edit to the holding
            if response["holding"] == []:
                print(
                    "holding_id %s doesn't exist, or user %s not allowed",
                    holding_id,
                    user_id,
                )
                raise GraphQLIacsObjectNotFoundError()

    @staticmethod
    async def get_parcelles(numero_national, producteur_id):
        """
        Retrieve plots from IACS api.
        Raise specific custom exceptions to notify user if problem

        Raises:
            GraphQLIacsServiceError: an error occurred on the side of the current IACS service
            GraphQLIacsUserForbiddenError: the user is not allowed to query the requested data
            GraphQLIacsObjectNotFoundError: the user does not exist
            GraphQLIacsRemoteError: an error occurred on the side of the paying agency API
        """

        with tracer().start_as_current_span("query_get_parcelles"):
            print("opw_query_get_parcelles")

            try:
                data = await opw_client.get_parcelles(
                    numero_national=numero_national,
                    producteur_id=producteur_id,
                )
            except httpx.HTTPStatusError as ex:
                status_code: codes = ex.response.status_code
                print(
                    """HTTPStatusError: %s
                    on get_parcelles(numero_national= %s, producteur_id= %s)""",
                    status_code,
                    numero_national,
                    producteur_id,
                )
                if status_code in [
                    httpx.codes.BAD_REQUEST,  # 400
                    httpx.codes.UNAUTHORIZED,  # 401
                ]:
                    raise GraphQLIacsServiceError() from ex

                if status_code == httpx.codes.FORBIDDEN:  # 403
                    raise GraphQLIacsUserForbiddenError() from ex

                if status_code == httpx.codes.NOT_FOUND:  # 404
                    raise GraphQLIacsObjectNotFoundError() from ex

                raise GraphQLIacsRemoteError() from ex  # 500, unexpected

            if not isinstance(data, list):
                print("Unexpected data returned: %s", data)
                raise GraphQLIacsRemoteError()

            if not data:
                logger.warning(
                    "No data found on get_parcelles(numero_national= %s, producteur_id= %s)",
                    numero_national,
                    producteur_id,
                )

            return data

    @staticmethod
    def format_plots(data, messages: list, message_base_id):
        """
        Parse plots as returned from IACS api into the fast ontology
        """
        with tracer().start_as_current_span("format_plots"):
            print("opw_format_plots")

            plots = []
            plots_not_inserted = []

            for parcelle in data:

                try:
                    geometry = wkt.loads(parcelle["geometrie"])

                    # Reproject
                    geometry = ops.transform(project_opw_to_4258, geometry)

                    # Convert the polygon to a MultiPolygon (geometry type for Plot in the database)
                    geometry = MultiPolygon([geometry])

                    # Convert to a __geo_interface__ dict and assign CRS
                    geometry = geometry.__geo_interface__
                    geometry["crs"] = {
                        "type": "name",
                        "properties": {"name": "EPSG:4258"},
                    }

                    plant_variety_id = (
                        None
                        if not parcelle["cultureCode"]
                        else str(parcelle["cultureCode"])
                    )

                    parcelle_id = str(parcelle["id"])
                    if len(parcelle_id) > 256:
                        plots_not_inserted.append(parcelle["id"])
                        continue

                    parcelle_name = str(parcelle["nom"])[:256]

                    plots += [
                        {
                            "authority_id": parcelle_id,
                            "name": parcelle_name,
                            "geometry": geometry,
                            "plant_variety_id": plant_variety_id,
                        }
                    ]
                except Exception as ex:
                    print(
                        "Data improperly formatted for parcelle: %s %s", parcelle, ex
                    )
                    plots_not_inserted.append(parcelle["id"])

            if plots_not_inserted:
                message_id = deepcopy(message_base_id)
                message_id["plots_not_inserted"] = len(plots_not_inserted)
                message = IACSMessage(
                    id=hashlib.md5(str(message_id).encode()).hexdigest()
                )

                message.title = config.NOT_ALL_PLOTS_INSERTED_MESSAGE.format(
                    PLOT_COUNT=str(len(plots_not_inserted))
                )
                message.detail = ", ".join(
                    map(
                        str,
                        plots_not_inserted[0 : config.PLOTS_IN_MESSAGE_TRUNCATE_AFTER],
                    )
                )
                if len(plots_not_inserted) > config.PLOTS_IN_MESSAGE_TRUNCATE_AFTER:
                    message.detail += ",…"

                messages.append(message)

            return plots

    @staticmethod
    async def check_plant_varieties_exist(plots, messages, message_base_id):
        """
        Make sure all plant varieties exist in fast db.
        Remove any that don't, since it would block the entire mutation.
        """
        with tracer().start_as_current_span("check_plant_varieties_exist"):
            print("opw_check_plant_varieties_exist")

            all_plant_variety_ids = list(
                set([plot["plant_variety_id"] for plot in plots])
            )

            # Remove null cultureCodes
            # Since cultureCode is currently never set in api,
            # happens for every plot
            if None in all_plant_variety_ids:
                all_plant_variety_ids.remove(None)

            # Check that the plant varieties we need actually exist in the db
            query = (
                config.API_DIR / "graphql/query_plant_variety_in_list.graphql"
            ).read_text()
            variables = {
                "plant_variety_ids": all_plant_variety_ids,
            }

            response = await fastplatform.execute(
                gql(query),
                variables,
            )

            # Difference between what we requested and what we received back
            missing_plant_variety_ids = [
                pvi
                for pvi in all_plant_variety_ids
                if pvi not in [pv["id"] for pv in response["plant_variety"]]
            ]

            plots_with_culture_code_missing = []
            if missing_plant_variety_ids:
                print(
                    "Plant varieties %s returned by API, but do not exist in FaST database and were not imported.",
                    missing_plant_variety_ids,
                )
                for plot in plots:
                    # Remove plant_variety_unknown
                    if plot["plant_variety_id"] in missing_plant_variety_ids:
                        del plot["plant_variety_id"]

                    plots_with_culture_code_missing.append(plot["authority_id"])

            if plots_with_culture_code_missing:
                message_id = deepcopy(message_base_id)
                message_id["plots_with_culture_code_missing"] = len(
                    plots_with_culture_code_missing
                )
                message = IACSMessage(
                    id=hashlib.md5(str(message_id).encode()).hexdigest()
                )

                message.title = config.SOME_PLOTS_WITH_CULTURE_CODE_MISSING.format(
                    PLOT_COUNT=str(len(plots_with_culture_code_missing))
                )
                message.detail = ", ".join(
                    map(
                        str,
                        missing_plant_variety_ids[
                            0 : config.CULTURE_CODES_IN_MESSAGE_TRUNCATE_AFTER
                        ],
                    )
                )

                if (
                    len(plots_with_culture_code_missing)
                    > config.CULTURE_CODES_IN_MESSAGE_TRUNCATE_AFTER
                ):
                    message.detail += ",…"

                messages.append(message)

    @staticmethod
    def build_holding_campaigns(holding_id, campaign_id, plots):
        """
        Return a dict that contains the entire mutation to be executed by the graphql client
        """
        with tracer().start_as_current_span("build_upsert_holding_campaigns"):
            print("opw_build_upsert_holding_campaigns")

            # Build the GraphQL mutation data
            # TODO: remove plot_plant_varieties when this issue is fixed:
            # https://hasura.io/docs/latest/graphql/core/guides/data-modelling/one-to-one.html#current-limitations-with-nested-mutations
            try:
                holding_campaigns = [
                    {
                        "campaign_id": campaign_id,
                        "holding_id": holding_id,
                        "sites": {
                            "data": [
                                {
                                    "authority_id": holding_id,
                                    "name": holding_id,  # No holding name in current opw api
                                    "plots": {
                                        "data": [
                                            {
                                                "authority_id": plot["authority_id"],
                                                "name": plot["name"],
                                                "geometry": plot["geometry"],
                                                "plot_plant_varieties": {
                                                    "data": [
                                                        {
                                                            "plant_variety_id": plot[
                                                                "plant_variety_id"
                                                            ],
                                                        }
                                                    ],
                                                    "on_conflict": {
                                                        "constraint": "plot_plant_variety_plot_id_plant_variety_id_unique",
                                                        "update_columns": [
                                                            "plot_id",
                                                            "plant_variety_id",
                                                        ],
                                                    },
                                                },
                                            }
                                            for plot in plots
                                        ],
                                        "on_conflict": {
                                            "constraint": "plot_authority_id_site_id_unique",
                                            "update_columns": [
                                                "authority_id",
                                                "site_id",
                                                "name",
                                                "geometry",
                                            ],
                                        },
                                    },
                                }
                            ],
                            "on_conflict": {
                                "constraint": "site_authority_id_holding_campaign_id_unique",
                                "update_columns": [
                                    "authority_id",
                                    "holding_campaign_id",
                                    "name",
                                ],
                            },
                        },
                    }
                ]
            except Exception as ex:
                print("Plots unparsable: %s", ex)
                raise GraphQLIacsServiceError() from ex

            # Write out the objects, for debug
            # from pprint import pprint
            # import json

            # pprint(holding_campaigns, indent=2)
            # with open("holding_campaigns.json", "w") as f:
            #     f.write(json.dumps(holding_campaigns, indent=2))

            return holding_campaigns

    @staticmethod
    async def insert_holding_campaigns(holding_campaigns):
        with tracer().start_as_current_span("insert_holding_campaign"):
            print("opw_insert_holding_campaign")

            try:
                mutation = (
                    config.API_DIR / "graphql/mutation_insert_holding_campaigns.graphql"
                ).read_text()
                variables = {"holding_campaigns": holding_campaigns}

                await fastplatform.execute(
                    gql(mutation),
                    variables,
                )

            except Exception as ex:
                print("Error inserting holding campaigns: %s", ex)
                raise GraphQLIacsServiceError() from ex
