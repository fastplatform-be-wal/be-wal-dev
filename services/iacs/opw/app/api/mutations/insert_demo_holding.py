import json
import logging

import graphene
from gql import gql
from opentelemetry import trace
from app.db.graphql_clients import fastplatform
from app.settings import config
from app.api.errors import GraphQLIacsServiceError, GraphQLIacsObjectNotFoundError


# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class InsertDemoHolding(graphene.Mutation):
    """Insert a demo holding for the requesting user"""

    class Arguments:
        campaign_id = graphene.Int(required=False)

    holding_id = graphene.String()

    async def mutate(self, info, campaign_id=None):

        request = info.context["request"]

        user_id = InsertDemoHolding.get_x_hasura_user_id(request)

        if campaign_id is None:
            campaign_id = await InsertDemoHolding.get_current_campaign()

        holding = InsertDemoHolding.create_mutation(campaign_id, user_id)

        response = await InsertDemoHolding.insert_demo_holding(fastplatform, holding)

        return InsertDemoHolding(holding_id=str(response["insert_holding_one"]["id"]))

    @staticmethod
    def get_x_hasura_user_id(request):
        """
        This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header or role
        """
        try:
            return request.headers["X-Hasura-User-Id"]
        except AttributeError as ex:
            print(ex)
            raise GraphQLIacsServiceError() from ex

    @staticmethod
    async def get_current_campaign():
        with tracer().start_as_current_span("insert_demo_get_current_campaign"):
            print("opw_insert_demo_get_current_campaign")
            try:
                query = (
                    config.API_DIR / "graphql/query_current_campaign.graphql"
                ).read_text()

                response = await fastplatform.execute(gql(query))
                campaign_id = response["campaign"][0]["id"]

            except AttributeError as ex:
                raise GraphQLIacsObjectNotFoundError() from ex

            return campaign_id

    @staticmethod
    def create_mutation(campaign_id, user_id):
        holding = (config.API_DIR / "demo/holding.json").read_text()
        holding = holding.replace("{{ campaign_id }}", str(campaign_id))
        holding = holding.replace("{{ user_id }}", user_id)
        holding = json.loads(holding)

        return holding

    @staticmethod
    async def insert_demo_holding(fp_client, holding):
        """
        Insert the demo holding for this user
        Note that this will fail (due to foreign key constraint violation)
        if the user does not exist
        """

        with tracer().start_as_current_span("insert_demo_holding"):
            print("opw_insert_demo_holding")
            mutation = (
                config.API_DIR / "graphql/mutation_insert_demo_holding.graphql"
            ).read_text()
            response = await fp_client.execute(
                gql(mutation), variable_values={"holding": holding}
            )

            return response
