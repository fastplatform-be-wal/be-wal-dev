import logging

import graphene
import graphene.types.generic
import httpx
from gql import gql
from httpx import codes
from opentelemetry import trace
from app.api.errors import (
    GraphQLIacsObjectNotFoundError,
    GraphQLIacsRemoteError,
    GraphQLIacsServiceError,
    GraphQLIacsUserForbiddenError,
)
from app.api.lib.opw import opw_client
from app.api.types import IACSMessage
from app.db.graphql_clients import fastplatform
from app.settings import config


# Log
logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


# TODO: role currently left blank (null) in OPW API.
# Replace `opw_advisor_role` and `opw_farmer_role` with real values
# and use if for actual role mapping
# ROLE_MAPPING = {"opw_advisor_role": "advisor", "opw_farmer_role": "farmer"}

# TODO: Remove when role returned by OPW API
DEFAULT_USER_ROLE = "farmer"


class SyncHoldings(graphene.Mutation):
    """Mutation for synchronizing the holdings of a user

    Expects the 'X-Hasura-User-Id' header to be set by the authentication hook

    Returns:
        SyncHoldings: A GraphQL node containing ok, errors and the holding_ids of the
                      farms that have been created/updated for the user
    """

    class Arguments:
        pass

    holding_ids = graphene.List(graphene.String)
    messages = graphene.List(graphene.NonNull(IACSMessage))

    async def mutate(self, info):
        """
        Mutation for synchronizing the holdings of a user
        as per values returned from IACS API.
        """

        request = info.context["request"]

        messages = []

        user_id = SyncHoldings.get_x_hasura_user_id(request)

        data = await SyncHoldings.get_producteurs(user_id)

        holdings = SyncHoldings.build_holding_upsert(user_id, data)
        if not holdings:
            raise GraphQLIacsObjectNotFoundError()

        await SyncHoldings.insert_holdings(holdings=holdings)

        return SyncHoldings(
            holding_ids=sorted([o["id"] for o in holdings]), messages=messages
        )

    @staticmethod
    def get_x_hasura_user_id(request):
        # This will intentionally fail if Hasura has not injected the X-Hasura-User-Id header
        print("get_x_hasura_user_id")
        try:
            return request.headers["X-Hasura-User-Id"]
        except AttributeError as ex:
            print(ex)
            raise GraphQLIacsServiceError() from ex

    @staticmethod
    async def get_producteurs(numero_national):
        """
        Retrieve the list of farms the user is allowed to see

        Args:
            user_id (str): The user_id whose farms we are querying in remote API.

        Raises:
            GraphQLIacsServiceError: an error occurred on the side of the current IACS service
            GraphQLIacsUserForbiddenError: the user is not allowed to query the requested data
            GraphQLIacsObjectNotFoundError: the user does not exist
            GraphQLIacsRemoteError: an error occurred on the side of the paying agency API

        Returns:
            dict: response.json(): a dict representing the content
        """
        print("opw_query_get_producteurs")

        with tracer().start_as_current_span("query_get_producteurs"):
            try:
                data = await opw_client.get_producteurs(numero_national=numero_national)
            except httpx.HTTPStatusError as ex:
                status_code: codes = ex.response.status_code
                print(
                    "HTTPStatusError:%s on get_producteurs(numero_national=%s)",
                    status_code,
                    numero_national,
                )
                if status_code in [
                    httpx.codes.BAD_REQUEST,  # 400
                    httpx.codes.UNAUTHORIZED,  # 401
                ]:
                    raise GraphQLIacsServiceError() from ex

                if status_code == httpx.codes.FORBIDDEN:  # 403
                    raise GraphQLIacsUserForbiddenError() from ex

                if status_code == httpx.codes.NOT_FOUND:  # 404
                    raise GraphQLIacsObjectNotFoundError() from ex

                raise GraphQLIacsRemoteError() from ex  # 500, unexpected

            if not isinstance(data, list) or not data:
                print(
                    "No data found on get_producteurs(numero_national=%s)",
                    numero_national,
                )
                raise GraphQLIacsRemoteError()

            return data

    @staticmethod
    def build_holding_upsert(user_id, data):
        """Build the list of holdings that will be upserted

        Args:
            user_id (str): The user_id whose farms we are querying in remote API
            data (dict): The data returned in response

        Raises:
            GraphQLIacsServiceError:
                an error occurred on the side of the current IACS service,
                during parsing.

        Returns:
            list: a list of graphql inserts to be performed
        """
        print("opw_build_holdings_upsert")
        with tracer().start_as_current_span("build_holdings_upsert"):
            try:
                # Filter out invalid ids
                filtered = []
                for item in data:
                    if "name" not in item or not item["name"]:
                        item["name"] = str(item["id"])[:256]

                    if len(str(item["id"])) > 256:
                        logger.warning(
                            "holding_id > 256 chars not inserted: %s", str(item["id"])
                        )
                    else:
                        filtered.append(item)

                # Convert items tp upsert statements
                holdings = [
                    {
                        "id": str(item["id"]),
                        "name": item["name"],
                        "user_related_parties": {
                            "data": [
                                {
                                    "role": DEFAULT_USER_ROLE,  # TODO: role mapping when API returns role
                                    "user_id": user_id,
                                }
                            ],
                            "on_conflict": {
                                "constraint": "user_related_party_holding_id_user_id_unique",
                                "update_columns": ["role"],
                            },
                        },
                    }
                    for item in filtered
                ]
            except Exception as ex:
                print("%s: Data improperly formatted: %s", ex, data)
                raise GraphQLIacsServiceError() from ex

            return holdings

    @staticmethod
    async def insert_holdings(holdings):
        """
        Insert the holdings and set this user as a related party.
        Note that this will fail (due to foreign key constraint violation)
        if the user does not exist

        Args:
            holdings (list): a list of graphql inserts to be performed

        Raises:
            GraphQLIacsServiceError: an error occurred on the side of the current IACS service
        """

        with tracer().start_as_current_span("insert_holdings"):
            print("opw_insert_holdings")
            try:
                mutation = (
                    config.API_DIR / "graphql/mutation_insert_holdings.graphql"
                ).read_text()

                response = await fastplatform.execute(
                    gql(mutation),
                    {"objects": holdings},
                )

                print("mutation_insert_holdings: %s", response)

            except Exception as ex:
                print("Error inserting holdings: %s", ex)
                raise GraphQLIacsServiceError() from ex
