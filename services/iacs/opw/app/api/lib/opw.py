import logging
from datetime import datetime, timedelta, timezone

import httpx
import jwt
from app.settings import config

import httpx
import ssl


logger = logging.getLogger(__name__)


class OPWClient:
    def __init__(self):
        self.access_token = None
        self.http_client = None

    def create_http_client(self):
        """Create an async HTTP client that will be used to make all the requests to
        the OPW API
        """
        ssl_context = httpx.create_ssl_context(
            verify=str(config.API_DIR / "lib/globalsign_rsa_ov_ssl_ca_2018.pem")
        )
        ssl_context.verify_mode = ssl.CERT_REQUIRED
        self.http_client = httpx.AsyncClient(verify=ssl_context)
        print("Http client created")

    async def close_http_client(self):
        """Close the HTTP client"""
        await self.http_client.aclose()

    async def get_producteurs(self, numero_national):
        """Get the 'producteurs' (i.e., the holdings/farms) for a given 'numéro
        national' (i.e., a user)

        Args:
            numero_national (str): The user's 'numéro national'

        Returns:
            dict: The OPW API response content
        """
        url = config.OPW_API_GET_PRODUCTEURS_URL.format(
            OPW_API_ROOT=config.OPW_API_ROOT,
            NUMERO_NATIONAL=numero_national,
        )

        data = await self.get_data(url)
        print("OPW client - get_producteurs - data - ", data)
        return data

    async def get_parcelles(self, producteur_id, numero_national):
        """Get the 'parcelles' (i.e., the plots) for a given 'producteur' (i.e., a
        holding) of a given 'numéro national' (i.e., a user)

        Args:
            producteur_id (str): Producteur identifier / holding_id
            numero_national (str): Numéro national / user_id

        Returns:
            dict: The OPW API response content
        """

        url = config.OPW_API_GET_PARCELLES_URL.format(
            OPW_API_ROOT=config.OPW_API_ROOT,
            PRODUCTEUR_ID=producteur_id,
            NUMERO_NATIONAL=numero_national,
        )

        data = await self.get_data(url)
        print("OPW client - get_parcelles - data - ", data)
        return data

    async def get_data(self, url):
        """Send a request to a URL and return the response content, while refreshing
        the access token if needed.
        """
        response = await self.send_request(url)

        # 401
        if response.status_code == httpx.codes.UNAUTHORIZED:
            # 401 -> Acccess unauthorized (token JWT absent, token JWT incorrect, token JWT expired,…)
            print(
                "HTTPStatusError:401 on get_data(), first attempt, refreshing token."
            )
            self.refresh_access_token()
            response = await self.send_request(url)

            # Check can access resource
            if response.status_code == httpx.codes.UNAUTHORIZED:
                # 401 -> Acccess unauthorized (service access not allowed,…)
                print(
                    "HTTPStatusError:401 on get_data(), second attempt, for url: %s",
                    url,
                )

        response.raise_for_status()
        data = response.json()

        return data

    async def send_request(self, url) -> httpx.Response:
        response = await self.http_client.get(
            url,
            headers={
                "Authorization": f"Bearer {self.access_token}",
                "Accept": "application/json",
            },
            timeout=config.OPW_TIMEOUT,
        )

        return response

    def refresh_access_token(self):
        """Refresh the OAuth access token of FaST"""

        now = datetime.now(tz=timezone.utc)

        issued_at = datetime.timestamp(now)
        expiration_time = datetime.timestamp(
            now + timedelta(seconds=config.OPW_TOKEN_VALIDITY_PERIOD)
        )
        issuer = config.OPW_KID

        self.access_token = jwt.encode(
            payload={
                "exp": int(expiration_time),
                "iat": int(issued_at),
                "iss": issuer,
                "acr": "opw",
            },
            key=config.OPW_SECRET_KEY,
            algorithm=config.OPW_JWT_SIGNING_ALGORITHM,
            headers={
                "kid": config.OPW_KID,
            },
        ).decode("utf-8")

        print("Token refreshed")


opw_client = OPWClient()
