import graphene
import logging

from app.api.queries.health import HealthQuery
from app.api.queries.name import NameQuery

logger = logging.getLogger(__name__)


class Query(HealthQuery, NameQuery, graphene.ObjectType):
    name_query = graphene.Field(NameQuery)
    health_query = graphene.Field(HealthQuery)

    def resolve_name_query(self, info):
        return NameQuery()

    def resolve_health_query(self, info):
        return HealthQuery()
